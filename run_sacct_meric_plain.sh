#!/bin/sh

#SBATCH --time=16:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --tasks-per-node=24
#SBATCH --cpus-per-task=1
#SBATCH --exclusive
#SBATCH --partition=haswell
#SBATCH --comment="no_monitoring"
#SBATCH --mem-per-cpu=2500M
#SBATCH -J "qe_os_meric"
#SBATCH -A p_readex
##SBATCH --reservation=READEX
#SBATCH --output=qe_ori_sacct_meric.out
#SBATCH --error=qe_ori_sacct_meric.out

#####
# set to number of times to repeat experiments
REPEAT_COUNT=1
#####

#LOC=$(pwd)
#cd ..

module purge
source ./readex_env/set_env_plain.source
##source $LOC/set_env_bem4i.source
cd input_data_small
i=1
rm -rf PLAIN_meric_v7_*
while [ $i -le $REPEAT_COUNT ]; do
  mkdir PLAIN_meric_v7_$i
  export MEASURE_RAPL_TARGET="PLAIN_meric_v7_$i"
   srun -N 1 -n 24 measure-rapl ${PWD}/../qe-6.1-scorep.noInst/PW/src/pw.x -i pw.in
  i=$(echo "$i + 1" | bc)
done
cd ..
module purge
source ./readex_env/set_env_meric.source
#source $LOC/set_env_bem4i.source
cd input_data_small
# SET MERIC OUTPUT
#export MERIC_COUNTERS=perfevent
export MERIC_MODE=3
export MERIC_DEBUG=0
export MERIC_CONTINUAL=1
export MERIC_AGGREGATE=0
export MERIC_DETAILED=0
export MERIC_NUM_THREADS=0
export MERIC_FREQUENCY=0
export MERIC_UNCORE_FREQUENCY=0
export MERIC_REGION_OPTIONS=./Energy_v3_meric.opts


i=1
rm -rf TUNED_meric_v7_*
while [ $i -le $REPEAT_COUNT ]; do
  mkdir TUNED_meric_v7_$i
  export MEASURE_RAPL_TARGET="TUNED_meric_v7_$i"
  srun -N 1 -n 24 measure-rapl ${PWD}/../qe-6.1-scorep.noInst/PW/src/pw.x -i pw.in
  i=$(echo "$i + 1" | bc)
done

i=1
total_time_plain=0
total_energy_plain=0
total_cpu_energy_plain=0
while [ $i -lt $REPEAT_COUNT ]; do
  times_energys=$(sacct -j $SLURM_JOBID.$i --format="JobID,CPUTimeRAW,ConsumedEnergyRaw")
  i=$(echo "$i + 1" | bc)
  times_energys_array=(${times_energys[@]})
  time_step=${times_energys_array[7]}
  energy_step=${times_energys_array[8]}
  total_time_plain=$(echo "${total_time_plain} + ${time_step}" | bc)
  total_energy_plain=$(echo "${total_energy_plain} + ${energy_step}" | bc)
  for file in PLAIN_meric_v7_$i/*
  do
    values=$( tail -1 $file | awk -F'[ ,]' '{print int($1)" "int($2)}' )
    values=(${values[@]})
    total_cpu_energy_plain=$[ total_cpu_energy_plain + ${values[0]} + ${values[1]} ]
  done
done

i=1
total_time_rrl=0
total_energy_rrl=0
total_cpu_energy_rrl=0
while [ $i -lt $REPEAT_COUNT ]; do
  times_energys=$(sacct -j $SLURM_JOBID.$((i+REPEAT_COUNT)) --format="JobID,CPUTimeRAW,ConsumedEnergyRaw")
  i=$(echo "$i + 1" | bc)
  times_energys_array=(${times_energys[@]})
  time_step=${times_energys_array[7]}
  energy_step=${times_energys_array[8]}
  total_time_rrl=$(echo "${total_time_rrl} + ${time_step}" | bc)
  total_energy_rrl=$(echo "${total_energy_rrl} + ${energy_step}" | bc)
  for file in TUNED_meric_v7_$i/*
  do
    values=$( tail -1 $file | awk -F'[ ,]' '{print int($1)" "int($2)}' )
    values=(${values[@]})
    total_cpu_energy_rrl=$[ total_cpu_energy_rrl + ${values[0]} + ${values[1]} ]
  done
done

echo ""
echo "Total Plain Time = $total_time_plain, Total Plain Energy = $total_energy_plain"
echo "Total MERIC Time = $total_time_rrl, Total MERIC Energy = $total_energy_rrl"

echo "" >> SACCT_meric_results.log
echo "Total Plain Time = $total_time_plain, Total Plain Energy = $total_energy_plain" >> SACCT_meric_results.log
echo "Total MERIC Time = $total_time_rrl, Total MERIC Energy = $total_energy_rrl" >> SACCT_meric_results.log


avg_time_plain=$(echo "$total_time_plain / $((REPEAT_COUNT-1))" | bc)
avg_energy_plain=$(echo "$total_energy_plain / $((REPEAT_COUNT-1))" | bc)
avg_time_rrl=$(echo "$total_time_rrl / $((REPEAT_COUNT-1))" | bc)
avg_energy_rrl=$(echo "$total_energy_rrl / $((REPEAT_COUNT-1))" | bc)
avg_cpu_energy_plain=$(echo "$total_cpu_energy_plain / $((REPEAT_COUNT-1))" | bc)
avg_cpu_energy_rrl=$(echo "$total_cpu_energy_rrl / $((REPEAT_COUNT-1))" | bc)

echo ""
echo "Average Plain Time = $avg_time_plain, Average MERIC Time = $avg_time_rrl"
echo "Average Plain Energy = $avg_energy_plain, Average MERIC Energy = $avg_energy_rrl"
echo "Average Plain CPU Energy = $avg_cpu_energy_plain, Average MERIC CPU Energy = $avg_cpu_energy_rrl"

echo ""
echo "Average Plain Time = $avg_time_plain, Average MERIC Time = $avg_time_rrl" >> SACCT_meric_results.log
echo "Average Plain Energy = $avg_energy_plain, Average MERIC Energy = $avg_energy_rrl" >> SACCT_meric_results.log
echo "Average Plain CPU Energy = $avg_cpu_energy_plain, Average MERIC CPU Energy = $avg_cpu_energy_rrl" >> SACCT_meric_results.log

cp -r PLAIN_meric_v7_* ../.
cp -r TUNED_meric_v7_* ../.
cp SACCT_meric_results ../.
#rm -rf PLAIN_meric_v7_*
#rm -rf TUNED_meric_v7_*

