#!/bin/sh

#SBATCH --time=16:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --tasks-per-node=24
#SBATCH --cpus-per-task=1
#SBATCH --exclusive
#SBATCH --partition=haswell
#SBATCH --comment="no_monitoring"
#SBATCH --mem-per-cpu=2500M
#SBATCH -J "qe_s_rrl"
#SBATCH -A p_readex
##SBATCH --reservation=READEX
#SBATCH --output=qe_sacct_rrl.out
#SBATCH --error=qe_sacct_rrl.out

#####
# set to number of times to repeat experiments
REPEAT_COUNT=25
#####

#LOC=$(pwd)
#cd ..

module purge
source ./readex_env/set_env_plain.source
#source $LOC/set_env_bem4i.source

export SCOREP_ENABLE_PROFILING="false"
export SCOREP_ENABLE_TRACING="false"
export SCOREP_SUBSTRATE_PLUGINS=""
export SCOREP_RRL_PLUGINS=""
export SCOREP_RRL_TMM_PATH=""
export SCOREP_MPI_ENABLE_GROUPS=ENV

cd input_data_small
i=1
rm -rf PLAIN_rrl_v9_*
while [ $i -le $REPEAT_COUNT ]; do
  mkdir PLAIN_rrl_v9_$i
  export MEASURE_RAPL_TARGET="PLAIN_rrl_v9_$i"
  srun -N 1 -n 24 measure-rapl ${PWD}/../qe-6.1-scorep.noInst/PW/src/pw.x -i pw.in
  i=$(echo "$i + 1" | bc)
done

cd ..
module purge
source ./readex_env/set_env_rrl.source
#source $LOC/set_env_bem4i.source
cd input_data_small

export SCOREP_ENABLE_PROFILING="false"
export SCOREP_ENABLE_TRACING="false"
export SCOREP_SUBSTRATE_PLUGINS="rrl"
export SCOREP_RRL_PLUGINS="cpu_freq_plugin,uncore_freq_plugin,OpenMPTP"
export SCOREP_RRL_TMM_PATH="tuning_model.json"
export SCOREP_MPI_ENABLE_GROUPS=ENV
export SCOREP_RRL_CHECK_IF_RESET="reset"

i=1
rm -rf TUNED_rrl_v9_*
while [ $i -le $REPEAT_COUNT ]; do
  mkdir TUNED_rrl_v9_$i
  export MEASURE_RAPL_TARGET="TUNED_rrl_v9_$i"
  srun -N 1 -n 24 measure-rapl ${PWD}/../qe-6.1-scorep.noInst/PW/src/pw.x -i pw.in
  i=$(echo "$i + 1" | bc)
done


REPEAT_COUNT=25
#####
i=1
LC_NUMERIC="de_CH.UTF-8" 
total_energy_plain=0
total_time_plain=0
while [ $i -lt $((REPEAT_COUNT + 1)) ]; do
	
	for file in PLAIN_rrl_v9_$i/*
  	do
	#echo $file
	#echo $i
    #values=$( tail -1 $file | awk -F'[ ,]' '{energy[$i]=$1+$2+$3+$4;time[$i]=$5;print "Energy:"energy[$i]; print "time:"time[$i] }' )
    energy_=$( tail -1 $file | awk -F'[ ,]' '{energy[$i]=$1+$2+$3+$4; print energy[$i] }' )
    time_=$( tail -1 $file | awk -F'[ ,]' '{time[$i]=$5; print time[$i] }' )
	echo $values
	echo $energy_
	echo $time_
	total_energy_plain=`echo $energy_ + $total_energy_plain | bc` 
	total_time_plain=`echo $time_ + $total_time_plain | bc` 
	echo "tot" $total_energy_plain
	
  done
	i=$((i+1))
done



#####
i=1
LC_NUMERIC="de_CH.UTF-8" 
total_energy_rrl=0
total_time_rrl=0
while [ $i -lt $((REPEAT_COUNT + 1)) ]; do
	
	for file in TUNED_rrl_v9_$i/*
  	do
	#echo $file
	#echo $i
    #values=$( tail -1 $file | awk -F'[ ,]' '{energy[$i]=$1+$2+$3+$4;time[$i]=$5;print "Energy:"energy[$i]; print "time:"time[$i] }' )
    energy_=$( tail -1 $file | awk -F'[ ,]' '{energy[$i]=$1+$2+$3+$4; print energy[$i] }' )
    time_=$( tail -1 $file | awk -F'[ ,]' '{time[$i]=$5; print time[$i] }' )
	echo $values
	echo $energy_
	echo $time_
	total_energy_rrl=`echo $energy_ + $total_energy_rrl | bc` 
	total_time_rrl=`echo $time_ + $total_time_rrl | bc` 
	echo "tot" $total_energy_rrl
	
  done
	i=$((i+1))
done

echo ""
echo "Total Plain Time = $total_time_plain, Total Plain Energy = $total_energy_plain"
echo "Total RRL Time = $total_time_rrl, Total RRL Energy = $total_energy_rrl"



avg_time_plain=$(echo "$total_time_plain / $((REPEAT_COUNT)) " | bc)
avg_energy_plain=$(echo "$total_energy_plain / $((REPEAT_COUNT))" | bc)
avg_time_rrl=$(echo "$total_time_rrl / $((REPEAT_COUNT))" | bc)
avg_energy_rrl=$(echo "$total_energy_rrl / $((REPEAT_COUNT))" | bc)
avg_cpu_energy_plain=$(echo "$total_cpu_energy_plain  / $((REPEAT_COUNT)) " | bc)
avg_cpu_energy_rrl=$(echo "$total_cpu_energy_rrl / $((REPEAT_COUNT))" | bc)

echo ""
echo "Average Plain Time = $avg_time_plain, Average RRL Time = $avg_time_rrl"
echo "Average Plain Energy = $avg_energy_plain, Average RRL Energy = $avg_energy_rrl"
#echo "Average Plain CPU Energy = $avg_cpu_energy_plain, Average RRL CPU Energy = $avg_cpu_energy_rrl"


cp -r PLAIN_rrl_v9_* ../.
cp -r TUNED_rrl_v9_* ../.
cp SACCT_RRL_results.log ../.
#rm -rf PLAIN_rrl_v9_*
#rm -rf TUNED_rrl_v9_*

