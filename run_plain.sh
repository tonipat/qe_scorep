#!/bin/bash

#SBATCH --time=1:00:00 # walltime
#SBATCH --nodes=1 # number of processor cores (i.e. tasks)
#SBATCH --ntasks-per-node=24
#SBATCH --cpus-per-task=1
#SBATCH --exclusive
#SBATCH --partition=haswell
#SBATCH --comment="no_monitoring"
#SBATCH --mem-per-cpu=2500M # memory per CPU core
#SBATCH -A p_readex
##SBATCH --reservation=READEX
#SBATCH -J "qe_plain" # job name
#SBATCH --output=qe_plain.out
#SBATCH --error=qe_plain.out
###############################################################################

module purge
source ./readex_env/set_env_plain.source
#source ./set_env_qe.source

cd input_data_small

srun --cpu_bind=verbose,cores --nodes 1 --ntasks-per-node 24 --cpus-per-task 1 -n 24 ../qe-6.1-scorep.noInst/PW/src/pw.x < pw.in 

