import collections
from collections import OrderedDict as ordDict

root_folder = [('/home/portero/sw/quantum_esp/qe_readex_git/qe_meric_dir_v2', None)]

main_reg = [{'Main-PWSCF': 'Main-PWSCF'}]

y_label = ordDict((('Job info - HDEEM', [('AVG Runtime of function [s]', 's')]),
('COUNTERS - HDEEM:', [('SUM Energy consumption [J] (Stats structure)', 'J')])))

time_energy_vars = {'time': ('Job info - HDEEM', 'AVG Runtime of function [s]'),
'energy': ('COUNTERS - HDEEM:', 'SUM Energy consumption [J] (Stats structure)')}

file_name_args_tup = [
['config', 'node'],
['config', 'quantum espreso'],
['funcLabel', 'core freq'],
['xLabel', 'uncore freq']
]

smooth_runs_average = False

def_keys_vals = []
keys_units = []

def_label_val = 25
func_label_unit = 'Ghz/core'
label_val_multiplier = 0.1

def_x_val = 30
x_val_unit = 'Ghz/uncore'
x_val_multiplier = 0.1

all_nested_regs = ['addusdens_function', 'c_bands_function', 'cdiaghg_function', 'cegterg_function', 'diag_bands_k_function', 'electrons-electrons_scf_function', 'h_psi_body_function', 'h_psi_function', 'h_psi_in_add_vuspsi', 'h_psi_in_calbec_rs_gamma', 'h_psi_in_pot', 'h_psi_in_vloc_psi_k', 'h_psi_in_whole', 'init_us_22_function', 'mix_rho_function', 'newd_function', 'run-PWSCF', 'run-PWSCF-electrons_function', 's_psi_function', 'sum_band_function', 'v_of_rho_2_function', 'v_of_rho_3_function']

iter_call_region = 'Main-PWSCF'

detailed_info = False

optim_settings_file_path = None
generate_optim_settings_file = {'core freq' :'FREQUENCY',
'uncore freq' :'UNCORE_FREQUENCY'}

baseline = None

test_csv_init = False