#!/bin/bash

#SBATCH --time=1:00:00 # walltime
#SBATCH --nodes=1 # number of processor cores (i.e. tasks)
#SBATCH --ntasks-per-node=24
#SBATCH --cpus-per-task=1
#SBATCH --exclusive
#SBATCH --partition=haswell
#SBATCH --comment="no_monitoring"
#SBATCH --mem-per-cpu=2500M # memory per CPU core
#SBATCH -A p_readex
#SBATCH --reservation=READEX
#SBATCH -J "qe_saf" # job name
#SBATCH --output=qe_saf_manual.out
#SBATCH --error=qe_saf_manual.out
###############################################################################

module purge
source ./readex_env/set_env_saf.source
#source ./set_env_qe.source

cd input_data_small

export SCOREP_TOTAL_MEMORY=4000M
export SCOREP_FILTERING_FILE=scorep_manual.filt

rm -rf scorep-*
rm -f old_scorep_manual.filt
echo "" > scorep_manual.filt

if [ "$READEX_INTEL" == "1" ]; then
  rm -rf old_scorep_icc_manual.filt
  echo "" > scorep_icc_manual.filt
fi

#SAF_t=0.001 #1 ms
#SAF_t=0.01 #10 ms
SAF_t=0.1 #100 ms

result=1
while [ $result != 0 ]; do
  echo "result = "$result
  srun --cpu_bind=verbose,cores --nodes 1 --ntasks-per-node 24 --cpus-per-task 1 ../qe-6.1-scorep.Inst/PW/src/pw.x < pw.in 
  echo "Aplication run - done."
  ../do_scorep_autofilter_single.sh $SAF_t
  result=$?
 
#if [ "$READEX_INTEL" == "1" ] && [ $result != 0 ]; then
#  export FILTER_INTEL="-tcollect-filter=scorep_icc.filt"
#  ./compile_for_saf.sh
#fi

  echo "scorep_autofilter_single done ($result)."
done
 cp scorep_manual*.filt ../.

echo "end of scorep-autofilter."
