#!/bin/bash

#SBATCH --time=12:00:00 # walltime
#SBATCH --nodes=1 # number of processor cores (i.e. tasks)
#SBATCH --ntasks-per-node=24
#SBATCH --cpus-per-task=1
#SBATCH --exclusive
#SBATCH --partition=haswell
#SBATCH --comment="no_monitoring"
#SBATCH --mem-per-cpu=2500M # memory per CPU core
#SBATCH -A p_readex
##SBATCH --reservation=READEX
#SBATCH -J "qe_pprof" # job name
#SBATCH --output=qe_pprof.out
#SBATCH --error=qe_pprof.out
###############################################################################

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/portero/sw/gperftools/install/lib
export PPROF_PATH="/home/portero/sw/gperftools/install/lib/libprofiler.so"
module purge
#source ./readex_env/set_env_plain.source
source ./readex_env/set_env_meric.source
#source ./set_env_qe.source

cd input_data_small

export MERIC_OUTPUT_DIR="qe_meric_dir_plain"
  export MERIC_OUTPUT_FILENAME="qe_25_30"
      export MERIC_NUM_THREADS=1
      export MERIC_FREQUENCY=25
      export MERIC_UNCORE_FREQUENCY=30

#Profile with pprof
#CPUPROFILE=qe_pprof.out 
#CPUPROFILE=qe_pprof.prof srun --cpu_bind=verbose,cores --nodes 1 --ntasks-per-node 24 --cpus-per-task 1 -n 24 ../qe-6.1-meric/PW/src/pw.x -i pw.in qe_pprof.prof > qe_pprof.txt
CPUPROFILE=qe_pprof.out mpirun -n 2 ../qe-6.1-meric/PW/src/pw.x -i pw.in 
#Execute with pprof
#pprof --text ${PWD}../qe-6.1-meric/PW/src/pw.x -i pw.in qe_pprof.prof > qe_pprof.txt


