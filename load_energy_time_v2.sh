#!/bin/sh

#SBATCH --time=16:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --tasks-per-node=24
#SBATCH --cpus-per-task=1
#SBATCH --exclusive
#SBATCH --partition=haswell
#SBATCH --comment="no_monitoring"
#SBATCH --mem-per-cpu=2500M
#SBATCH -J "qe_s_rrl"
#SBATCH -A p_readex
##SBATCH --reservation=READEX
#SBATCH --output=qe_sacct_rrl.out
#SBATCH --error=qe_sacct_rrl.out

#####
# set to number of times to repeat experiments

div ()  # Arguments: dividend and divisor
{
        if [ $2 -eq 0 ]; then echo division by 0; exit; fi
        local p=12                            # precision
        local c=${c:-0}                       # precision counter
        local d=.                             # decimal separator
        local r=$(($1/$2)); echo -n $r        # result of division
        local m=$(($r*$2))
        [ $c -eq 0 ] && [ $m -ne $1 ] && echo -n $d
        [ $1 -eq $m ] || [ $c -eq $p ] && return
        local e=$(($1-$m))
        let c=c+1
        div $(($e*10)) $2
}

REPEAT_COUNT=25
#####
i=1
LC_NUMERIC="de_CH.UTF-8" 
total_energy_plain=0
total_time_plain=0
while [ $i -lt $((REPEAT_COUNT + 1)) ]; do
	
	for file in PLAIN_rrl_v9_$i/*
  	do
	#echo $file
	#echo "i: " $i
    #values=$( tail -1 $file | awk -F'[ ,]' '{energy[$i]=$1+$2+$3+$4;time[$i]=$5;print "Energy:"energy[$i]; print "time:"time[$i] }' )
    energy_=$( tail -1 $file | awk -F'[ ,]' '{energy[$i]=$1+$2+$3+$4; print energy[$i] }' )
    time_=$( tail -1 $file | awk -F'[ ,]' '{time[$i]=$5; print time[$i] }' )
	energy[$i]=$energy_
	time[$i]=$time_	
	#echo $values
	#echo $energy_
	#echo $time_
	total_energy_plain=`echo ${energy[$i]} + $total_energy_plain | bc`
	total_time_plain=`echo ${time[$i]} + $total_time_plain | bc` 
	#echo "tot plain" $total_energy_plain
	
  done
	i=$((i+1))
done


avg_time_plain=$(echo "$total_time_plain / $((REPEAT_COUNT)) " | bc)
avg_energy_plain=$(echo "$total_energy_plain / $((REPEAT_COUNT))" | bc)

#Sigma Energy computation PLAIN
exp_e_suma_plain=0
for  j in `seq 1 $((REPEAT_COUNT))`; do
	#echo "j :"$j
	suma_e_plain=`echo ${energy[$j]} - $avg_energy_plain | bc` 
	exp_e_plain=$(echo "${suma_e_plain}^2" | bc)     
	exp_e_suma_plain=`echo $exp_e_suma_plain + $exp_e_plain | bc` 
	#echo "suma plain: "$suma_e_plain" "$exp_e_plain" "$exp_e_suma_plain
done

sigma_e_exp=$(echo "$total_energy_plain / $((REPEAT_COUNT))" | bc)
sigma_e_plain=$(echo "sqrt($sigma_e_exp)" | bc)
three=3
sigma3_e_plain=$(( $sigma_e_plain * $three))



#Sigma Time computation PLAIN
exp_t_suma_plain=0
for  j in `seq 1 $((REPEAT_COUNT))`; do
	#echo "j :"$j
	suma_t_plain=`echo ${time[$j]} - $avg_time_plain | bc` 
	exp_t_plain=$(echo "${suma_t_plain}^2" | bc)     
	exp_t_suma_plain=`echo $exp_t_suma_plain + $exp_t_plain | bc` 
	#echo "suma plain: "$suma_t_plain" "$exp_t_plain" "$exp_t_suma_plain
done

sigma_t_exp=$(echo "$total_time_plain / $((REPEAT_COUNT))" | bc)
sigma_t_plain=$(echo "sqrt($sigma_t_exp)" | bc)
three=3
sigma3_t_plain=$(( $sigma_t_plain * $three)) 
echo " energy 3sigma"$sigma3_e_plain
echo " ime 3sigma"$sigma3_t_plain

#REPEAT_COUNT=25
#####
i=1
total_energy_rrl=0
total_time_rrl=0
while [ $i -lt $((REPEAT_COUNT + 1)) ]; do
	
	for file in TUNED_rrl_v9_$i/*
  	do
	#echo $file
	#echo $i
   # values=$( tail -1 $file | awk -F'[ ,]' '{energy[$i]=$1+$2+$3+$4;time[$i]=$5;print "Energy:"energy[$i]; print "time:"time[$i] }' )
    energy_=$( tail -1 $file | awk -F'[ ,]' '{energy[$i]=$1+$2+$3+$4; print energy[$i] }' )
    time_=$( tail -1 $file | awk -F'[ ,]' '{time[$i]=$5; print time[$i] }' )
	#echo "vslurd " $values
	energy[$i]=$energy_
	time[$i]=$time_
	#echo "Energy: "${energy[$i]}
	#echo "Time: "${time[$i]}
	total_energy_rrl=`echo ${energy[$i]} + $total_energy_rrl | bc` 
	total_time_rrl=`echo ${time[$i]} + $total_time_rrl | bc` 
	#echo "tot" $total_energy_rrl
	
  done
	i=$((i+1))
done

avg_time_rrl=$(echo "$total_time_rrl / $((REPEAT_COUNT))" | bc)
avg_energy_rrl=$(echo "$total_energy_rrl / $((REPEAT_COUNT))" | bc)

#Sigma Energy computation RRL
exp_e_suma_rrl=0
for  j in `seq 1 $((REPEAT_COUNT))`; do
	#echo "j :"$j
	suma_e_rrl=`echo ${energy[$j]} - $avg_energy_rrl | bc` 
	exp_e_rrl=$(echo "${suma_e_rrl}^2" | bc)     
	exp_e_suma_rrl=`echo $exp_e_suma_rrl + $exp_e_rrl | bc` 
	#echo "suma rrl: "$suma_e_rrl" "$exp_e_rrl" "$exp_e_suma_rrl
done

sigma_e_exp=$(echo "$total_energy_rrl / $((REPEAT_COUNT))" | bc)
sigma_e_rrl=$(echo "sqrt($sigma_e_exp)" | bc)
three=3
sigma3_e_rrl=$(( $sigma_e_rrl * $three)) 

#Sigma Time computation RRL
exp_t_suma_rrl=0
for  j in `seq 1 $((REPEAT_COUNT))`; do
	#echo "j :"$j
	suma_t_rrl=`echo ${time[$j]} - $avg_time_rrl | bc` 
	exp_t_rrl=$(echo "${suma_t_rrl}^2" | bc)     
	exp_t_suma_rrl=`echo $exp_t_suma_rrl + $exp_t_rrl | bc` 
	#echo "suma rrl: "$suma_t_rrl" "$exp_t_rrl" "$exp_t_suma_rrl
done

sigma_t_exp=$(echo "$total_time_rrl / $((REPEAT_COUNT))" | bc)
sigma_t_rrl=$(echo "sqrt($sigma_t_exp)" | bc)
three=3
sigma3_t_rrl=$(( $sigma_t_rrl * $three)) 


#echo "sigma rrl: "$sigma_t_rrl
#echo "Three sigma: "$sigma3_t_rrl
 



avg_time_rrl=$(echo "$total_time_rrl / $((REPEAT_COUNT))" | bc)
avg_energy_rrl=$(echo "$total_energy_rrl / $((REPEAT_COUNT))" | bc)
#avg_cpu_energy_plain=$(echo "$total_cpu_energy_plain " | bc)
#avg_cpu_energy_rrl=$(echo "$total_cpu_energy_rrl / $((REPEAT_COUNT))" | bc)

#Energy savings
#echo "Ev:"$avg_energy_plain
Esavings_rrl=$(echo "$avg_energy_plain - $avg_energy_rrl " | bc)
Esavings_rrl=$(echo "$(div $Esavings_rrl  $avg_energy_plain ) * 100" | bc)
Esavings_3_sigma_rrl=$(echo "$(div $sigma3_e_rrl $avg_energy_plain ) * 100" | bc)
#echo "EVs"$Esavings_rrl, "sigma "$Esavings_3_sigma_rrl
echo ""
echo "Total Plain Time = $total_time_plain, Total Plain Energy = $total_energy_plain"
echo "Total RRL Time = $total_time_rrl, Total RRL Energy  = $total_energy_rrl"

echo ""
echo "Average Plain Time = $avg_time_plain ± $sigma3_t_plain 3σ, Average RRL Time = $avg_time_rrl ± $sigma3_t_rrl 3σ (3sigma)"
echo "Average Plain Energy = $avg_energy_plain ± $sigma3_e_plain 3σ, Average RRL Energy = $avg_energy_rrl ± $sigma3_e_rrl 3σ (3sigma) "
echo "Energy savings QE RRL: $Esavings_rrl ± $Esavings_3_sigma_rrl (3σ)"
#echo "Average Plain CPU Energy = $avg_cpu_energy_plain, Average RRL CPU Energy = $avg_cpu_energy_rrl"



cp -r PLAIN_rrl_v9_* ../.
cp -r TUNED_rrl_v9_* ../.
cp SACCT_RRL_results.log ../.
#rm -rf PLAIN_rrl_v9_*
#rm -rf TUNED_rrl_v9_*

