#!/bin/bash

#SBATCH --time=72:00:00   # walltime
#SBATCH --nodes=1  # number of processor cores (i.e. tasks)
##SBATCH --ntasks=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=24
#SBATCH --exclusive
#SBATCH --partition=haswell
#SBATCH --comment="no_monitoring"
#SBATCH --mem-per-cpu=2500M   # memory per CPU core
#SBATCH -A p_readex
#SBATCH --reservation=READEX
#SBATCH -J "qe_meric"   # job name
#SBATCH --output=qe_pw_meric.out
#SBATCH --error=qe_pw_meric.out
###############################################################################

# LOAD MODULES
module purge
source ./readex_env/set_env_meric.source

cd input_data_small

export MERIC_OUTPUT_DIR="qe_meric_dir_pw_2_"
#rm -rf $MERIC_OUTPUT_DIR
#rm -rf ${MERIC_OUTPUT_DIR}Counters
#exit

echo $MERIC_OUTPUT_DIR
NUMA=""

# FOR EACH SETTINGS
#for thread in {24..4..-4}
#do
  for cpu_freq in {25..13..-4}
  do
    for uncore_freq in {30..14..-4}
    do
      export MERIC_OUTPUT_FILENAME="qe_"$cpu_freq"_"$uncore_freq
      export MERIC_NUM_THREADS=1
      export MERIC_FREQUENCY=$cpu_freq
      export MERIC_UNCORE_FREQUENCY=$uncore_freq

      echo
      echo TEST $MERIC_OUTPUT_FILENAME
#      for repeat in {1..1..1}
 #     do
   #     ret=1
    #    while [ "$ret" -ne 0 ]
     #   do
          srun -N 1 -n 1 -c 24 $NUMA ${PWD}/../qe-6.1-meric/PW/src/pw.x -i pw.in 
  #      done
     # done 
    done
  done
#done
mv ${MERIC_OUTPUT_DIR}Counters ../.
mv $MERIC_OUTPUT_DIR ../.


