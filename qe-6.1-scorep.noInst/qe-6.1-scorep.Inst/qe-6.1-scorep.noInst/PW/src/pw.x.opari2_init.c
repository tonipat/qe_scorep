

#ifdef __cplusplus
extern "C"
{
#endif
#include <stddef.h>

extern void pomp2_init_reg_jdkvgrn2fc7ca_1_();
extern void pomp2_init_reg_idfowo2fcyzs2_2_();
extern void pomp2_init_reg_jh2z0ro2fci7p2_8_();
extern void pomp2_init_reg_4t3oco2fcxd7h_1_();
extern void pomp2_init_reg_iuuevo2fcgz94_3_();
extern void pomp2_init_reg_15t1duo2fcmxnk_57_();
extern void pomp2_init_reg_p4t1dgo2fcufkb_17_();
extern void pomp2_init_reg_l0bfjzl2fchcf7_4_();
extern void pomp2_init_reg_ne0hlp2fcjajk_9_();
extern void pomp2_init_reg_gdfowo2fcul72_2_();
extern void pomp2_init_reg_g89icpm2fcgqda_7_();
extern void pomp2_init_reg_daifro2fcj2k3_5_();
extern void pomp2_init_reg_qykq1o2fcr9pg_1_();
extern void pomp2_init_reg_5u8pap2fcv2r2_4_();
extern void pomp2_init_reg_hf4ni1l2fchegc_1_();
extern void pomp2_init_reg_3aifsp2fc0hbl_3_();
extern void pomp2_init_reg_v3mpwm2fczw15_2_();
extern void pomp2_init_reg_kdfomo2fcg7ga_3_();

void POMP2_Init_regions()
{
    pomp2_init_reg_daifro2fcj2k3_5_();
    pomp2_init_reg_5u8pap2fcv2r2_4_();
    pomp2_init_reg_3aifsp2fc0hbl_3_();
    pomp2_init_reg_kdfomo2fcg7ga_3_();
    pomp2_init_reg_idfowo2fcyzs2_2_();
    pomp2_init_reg_jh2z0ro2fci7p2_8_();
    pomp2_init_reg_4t3oco2fcxd7h_1_();
    pomp2_init_reg_iuuevo2fcgz94_3_();
    pomp2_init_reg_15t1duo2fcmxnk_57_();
    pomp2_init_reg_p4t1dgo2fcufkb_17_();
    pomp2_init_reg_l0bfjzl2fchcf7_4_();
    pomp2_init_reg_ne0hlp2fcjajk_9_();
    pomp2_init_reg_gdfowo2fcul72_2_();
    pomp2_init_reg_g89icpm2fcgqda_7_();
    pomp2_init_reg_qykq1o2fcr9pg_1_();
    pomp2_init_reg_hf4ni1l2fchegc_1_();
    pomp2_init_reg_v3mpwm2fczw15_2_();
    pomp2_init_reg_jdkvgrn2fc7ca_1_();
}

size_t POMP2_Get_num_regions()
{
    return 130;
}


void POMP2_USER_Init_regions()
{
}

size_t POMP2_USER_Get_num_regions()
{
    return 0;
}

#ifdef __cplusplus
}
#endif
