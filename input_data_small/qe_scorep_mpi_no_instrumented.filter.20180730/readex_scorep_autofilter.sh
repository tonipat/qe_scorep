#!/bin/bash

PATH_2_cubex=$1
export PATH=$PATH:/home/portero/sw/readex-apps/readex-repository/env/intelmpi2017.2.174_intel2017.2.174:/sw/global/compilers/intel/2017/impi/2017.2.174/bin64

#. set_env_ptf.source
. set_env_ptf_hdeem.source
echo "pass the path to the foderl with profile.cubex: $1"
#Go to proper path: (i.e. scorep-<*data_and_time>)
scorep-autofilter -t 10 -f qe_filter_run_pwscf_ ${PATH_2_cubex}/profile.cubex 
