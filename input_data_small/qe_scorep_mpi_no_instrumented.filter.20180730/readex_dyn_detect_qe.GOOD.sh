#!/bin/bash

ml modenv/both
#export PATH=$PATH:/home/portero/sw/readex-apps/readex-repository/env/intelmpi2017.2.174_intel2017.2.174:/sw/global/compilers/intel/2017/impi/2017.2.174/bin64
export PATH=$PATH:/home/portero/sw/readex-apps/readex-repository/env/intelmpi2017.2.174_intel2017.2.174
#. set_env_ptf.source
.  set_env_rdd.source

#Go to proper path: (i.e. scorep-<*data_and_time>)
#readex-dyn-detect -t 0.1 -p MAIN__ -c 10 -v 10 -w 10 -r scorep -f report_qe ./profile.cubex
readex-dyn-detect -t 0.1 -p run_pwscf_ -c 10 -v 10 -w 10 -r scorep_run_pwscf_ -f report_qe_run_pwscf ./profile.cubex
