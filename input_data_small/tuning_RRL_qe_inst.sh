#!/bin/sh

#SBATCH --time=5:00:00#walltime 
#SBATCH --nodes=1 #numberofnodesrequested;1forPTFandremainingforapplicationrun 
#SBATCH --tasks-per-node=1 #numberofprocessespernodeforapplicationrun 
#SBATCH --cpus-per-task=24
#SBATCH --exclusive 
#SBATCH --partition=haswell 
#SBATCH --mem-per-cpu=2500M #memoryperCPUcore 10
#SBATCH -J "BRRL_qe_inst" #jobname 
#SBATCH -A p_readex
#SBATCH --comment="cpufreqchown"

echo "run RRL begin."

#Go to proper path:
#readex-dyn-detect -t 0.001 -p maintrace -c 1 -v 0.001 -w 1 -r scorep -f report_blender_client ./profile.cubex 

module --force purge


ml modenv/both
ml pdt/3.18.1
ml fftw/3.3.5-intel-xmpi

# This path must be updated when in production (and comment deleted)
ROOT_DIR=${PWD}
export PATH=$PATH:${ROOT_DIR}/home/portero/sw/readex-apps/readex-repository/env/intelmpi2017.2.174_intel2017.2.174:/sw/global/compilers/intel/2017/impi/2017.2.174/bin64

. set_env_rrl.source


export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/sw/global/compilers/intel/2017/mkl/lib/intel64:projects/p_readex/it4i/meric/lib:/projects/p_readex/it4i/meric/hdeem:/usr/local/lib:/sw/taurus/libraries/papi/5.4.3/lib:/usr/local/lib:/projects/p_readex/scorep/ci_TRY_READEX_online_access_call_tree_extensions_intelmpi2017.2.174_intel2017.2.174/lib

NP=1 #checkagainst--ntasksandtasks-per-node
PHASE_="maintrace"
ROOT_DIR=${PWD}


export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib  

echo "Start plain run"

export SCOREP_ENABLE_PROFILING="false" 
export SCOREP_ENABLE_TRACING="false" 
export SCOREP_SUBSTRATE_PLUGINS="" 
export SCOREP_RRL_PLUGINS="" 
export SCOREP_RRL_TMM_PATH="" 
export SCOREP_MPI_ENABLE_GROUPS=ENV

clearHdeem
echo "Plain Run" >> hdeem_qe_instrumented.out
startHdeem

mpirun -n 24 ${PWD}/../qe-6.1-scorep.Inst/PW/src/pw.x -i ZnO.in 

checkHdeem >> hdeem_qe_instrumented.out


echo "Start RRL-tuned run"
export SCOREP_SUBSTRATE_PLUGINS='rrl'
export SCOREP_RRL_VERBOSE="WARN"
export SCOREP_RRL_PLUGINS=cpu_freq_plugin,uncore_freq_plugin
export SCOREP_RRL_TMM_PATH=${ROOT_DIR}/tuning_model_qe_inst.zn.json
export SCOREP_ENABLE_TRACING=false
export SCOREP_ENABLE_PROFILING=false
export SCOREP_ENV_ENABLE_GROUPS=ENV
clearHdeem
echo "RRL-runed Run (1)" >> hdeem_qe_instrumented.out
startHdeem

mpirun -n 24 ${PWD}/../qe-6.1-scorep.Inst/PW/src/pw.x -i ZnO.in 

checkHdeem >> hdeem_qe_instrumented.out
echo "RRL-runed Run (2)" >> hdeem_qe_instrumented.out
clearHdeem
startHdeem

mpirun -n 24 ${PWD}/../qe-6.1-scorep.Inst/PW/src/pw.x -i ZnO.in 

stopHdeem
checkHdeem >> hdeem_qe_instrumented.out
echo "run RRL done."
# NO ACCESS
# cp /projects/p_readextest/miniMD/run_ptf.sh .
#cp: cannot stat `/projects/p_readextest/miniMD/run_ptf.sh': Permission denied
