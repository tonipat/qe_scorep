#!/bin/bash

cd /home/lriha/qe/input_data_small/

#ml load OpenMPI/2.1.1-GCC-6.3.0-2.27

ml load imkl/11.2.3.187
ml load FFTW/3.3.6-gompi-2017a
ml load LAPACK/3.7.1-gompi-2017a

#Allinea debugging and profiling module 
ml load Forge/7.1
export ALLINEA_SAMPLER_INTERVAL=1

#MERIC modules 
ml load x86_adapt
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/lriha/meric/lib

#QE variables 
export PREFIX=/home/lriha/qe/qe-6.1
export BIN_DIR=$PREFIX/bin
export PSEUDO_DIR=$PREFIX/pseudo


#MERIC variables 
export MERIC_FREQUENCY=25
export MERIC_UNCORE_FREQUENCY=30

export MERIC_OUTPUT_DIR="DELETEME_4"
export MERIC_NUM_THREADS=10
export MERIC_MODE=1 #0 HDEEM; 1 RAPL 
export MERIC_DETAILED=1
export MERIC_CONTINUAL=1

#$1 mpirun -n 2 --bind-to-socket $BIN_DIR/pw.x -i pw.in # > pw.out

export OMP_PROC_BIND=true
export OMP_PLACES=cores
mpirun -n 2 --bind-to-socket --map-by socket $BIN_DIR/pw.x -i pw.in # > pw.out

# SET MERIC OUTPUT #############################################################
export MERIC_OUTPUT_DIR="TEST_4"
#export MERIC_COUNTERS=perfevent
export MERIC_MODE=1
export MERIC_DETAILED=1
export MERIC_CONTINUAL=1
#export MERIC_AGGREGATE=0

# RUN THE CODE FOR EACH SETTINGS ###############################################
# export KMP_AFFINITY=compact
# export KMP_AFFINITY="granularity=core,scatter"

echo "RUNNING"
for proc in 2
do
	for thread in {12..2..2}
	do
		for cpu_freq in {25..12..2} # or 25 {24..12..2} or {33..12..2}
		do
			for uncore_freq in {30..12..2} # or {30..12..2}
			do
       			for repeat in 1 # {1..1..1}
       			do

					# OUTPUT FILE NAME
					export MERIC_OUTPUT_FILENAME=$thread"_"$cpu_freq"_"$uncore_freq

					# TEST SETTINGS
					export MERIC_NUM_THREADS=$thread
					export MERIC_FREQUENCY=$cpu_freq
					export MERIC_UNCORE_FREQUENCY=$uncore_freq
					
					export OMP_PROC_BIND=true
					export OMP_PLACES=cores
					
					# RUN THE TEST
					echo
					echo "TEST output:" $MERIC_OUTPUT_FILENAME 

					mpirun -n $proc --bind-to-socket --map-by socket $BIN_DIR/pw.x -i pw.in 

		 			# DO NOT USE THIS if, if you want more test with same settings <<<
					# ./blasTest

       			done
			done
		done
	done
done