#!/bin/bash

#!/bin/bash -l
#SBATCH --nodes=1
#SBATCH --tasks-per-node=1
#SBATCH --cpus-per-task=24
#SBATCH --time=0-02:00:00
#SBATCH -p haswell
#SBATCH -A p_readex
#SBATCH --reservation=READEX
#SBATCH --exclusive
#SBATCH --mem=5000
#SBATCH --comment="qe_scorep_test"

# Possible solution IMPORT tonipat@20180619: https://software.intel.com/es-es/forums/intel-fortran-compiler-for-linux-and-mac-os-x/topic/737584
module --force purge
ml modenv/both
ml pdt/3.18.1
#ml intel/2017a <==It has GCC. NO!
ml fftw/3.3.5-intel-xmpi
ml mkl
#ml glib
#ml intel
#ml scorep
module load intelmpi/2017.2.174
#ml load x86_adapt--> this is missing tonipat@20180613
#ml intel

#ml scorep
#ml intel/2017.2.174
#ml OpenBLAS/0.2.20-GCC-6.4.0-2.28 <--GCC NO
#ml ScaLAPACK/2.0.2-gompic-2018a-OpenBLAS-0.2.20 <==GCC NO!
ml list 

#PW/src/input.f90 313
. /home/portero/sw/readex-apps/readex-repository/env/intelmpi2017.2.174_intel2017.2.174/set_env_rdd.source


export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/sw/global/compilers/intel/2017/mkl/lib/intel64:/usr/local/lib:/usr/local/lib:/projects/p_readex/scorep/ci_TRY_READEX_online_access_call_tree_extensions_intelmpi2017.2.174_intel2017.2.174/lib
#ml scorep-4
#export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/portero/sw/scorep-4.0-install/lib
#export PATH=$PATH:/home/portero/sw/scorep-4.0-install/include


export SCOREP_VERBOSE=true
export SCOREP_TOTAL_MEMORY=4000M
export SCOREP_EXPERIMENT_DIRECTORY="./qe_scorep_mpi_instrumented_local"
export SCOREP_OVERWRITE_EXPERIMENT_DIRECTORY=true
export SCOREP_PROFILING_FORMAT=cube_tuple
#export SCOREP_ONLINEACCESS_ENABLE=true
export SCOREP_ENABLE_TRACING=false
export SCOREP_ENABLE_PROFILING=true
export SCOREP_PROFILING_ENABLE_CORE_FILES=false
#export SCOREP_PROFILING_ENABLE_CORE_FILES=1
export SCOREP_TRACING_COMPRESS=false
export SCOREP_METRIC_PAPI=PAPI_TOT_INS,PAPI_L3_TCM
#export SCOREP_METRIC_PAPI=PAPI_TOT_INS
export SCOREP_FILTERING_FILE=''
echo > scorep_scorep_mpi



export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/sw/global/compilers/intel/2017/mkl/lib/intel64:/usr/local/lib:/usr/local/lib:/projects/p_readex/scorep/ci_TRY_READEX_online_access_call_tree_extensions_intelmpi2017.2.174_intel2017.2.174/lib

#QE Variables
#export BIN_DIR=$PREFIX/bin
export BIN_DIR=/home/portero/sw/quantum_esp/qe_readex_git/qe-6.1-scorep.Inst/bin

proc=24
#proc=1

mpirun -n $proc $BIN_DIR/pw.x < pw.in 


#error:https://stackoverflow.com/questions/42309873/using-mpi-what-on-earth-is-execvp-error-on-file-error
#HYDU_create_process (../../utils/launch/launch.c:825): execvp error on file /bin/pw.x (No such file or directory)


