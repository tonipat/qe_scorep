#!/bin/bash -l

module --force purge


ml modenv/both
ml pdt/3.18.1

export PATH=$PATH:/home/portero/sw/readex-apps/readex-repository/env/intelmpi2017.2.174_intel2017.2.174:/sw/global/compilers/intel/2017/impi/2017.2.174/bin64
source set_env_rrl.source

export PATH=$PATH:./home/portero/sw/quantum_esp/qe_readex_master/input_data_small

readex-dyn-detect -t 0.01	\
	          -p mainpwscf \
		  -c 0.001	\
		  -v 0.5		\
		  -w 0.5		\
		  -r ./qe_scorep_mpi_instrumented \
	 	  -f ./qe_scorep_mpi_instrumented/report_qe \
		  ./qe_scorep_mpi_instrumented/profile.cubex
