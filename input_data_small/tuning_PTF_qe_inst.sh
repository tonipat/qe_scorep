#!/bin/sh

#SBATCH --time=24:00:00#walltime 
#SBATCH --nodes=2 
#SBATCH --tasks-per-node=24 #numberofprocessespernodeforapplicationrun 
#SBATCH --cpus-per-task=1 #completely different previous example: MPI=24, number of nodes 1. The other node is for monitoring
#SBATCH --exclusive 
#SBATCH --reservation=READEX
#SBATCH --partition=haswell 
#SBATCH --mem-per-cpu=2500M #memoryperCPUcore 10
#SBATCH -J "qe_inst_PTF" #jobname 
#SBATCH -A p_readex

echo "run PTF begin."

#Go to proper path:
#readex-dyn-detect -t 0.001 -p maintrace -c 1 -v 0.001 -w 1 -r scorep -f report_blender_client ./profile.cubex 

module --force purge


ml modenv/both
ml pdt/3.18.1

export PATH=$PATH:/home/portero/sw/readex-apps/readex-repository/env/intelmpi2017.2.174_intel2017.2.174:/sw/global/compilers/intel/2017/impi/2017.2.174/bin64

#. set_env_ptf.source
. set_env_ptf_hdeem.source

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/sw/global/compilers/intel/2017/mkl/lib/intel64:projects/p_readex/it4i/meric/lib:/projects/p_readex/it4i/meric/hdeem:/usr/local/lib:/sw/taurus/libraries/papi/5.4.3/lib:/usr/local/lib:/projects/p_readex/scorep/ci_TRY_READEX_online_access_call_tree_extensions_intelmpi2017.2.174_intel2017.2.174/lib

#module load scorep-hdeem/sync-hdeem2.2.5-intelmpi-intel2017
clearHdeem
#NP=1 #checkagainst--ntasksandtasks-per-node
PHASE_="mainpwscf"
ROOT_DIR=${PWD}

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib

  
export SCOREP_SUBSTRATE_PLUGINS=rrl 
export SCOREP_RRL_PLUGINS=cpu_freq_plugin,uncore_freq_plugin
export SCOREP_RRL_VERBOSE="WARN" 

export SCOREP_METRIC_PLUGINS=hdeem_sync_plugin
export SCOREP_METRIC_PLUGINS_SEP=";" 
export SCOREP_METRIC_HDEEM_SYNC_PLUGIN="*/E"
export SCOREP_METRIC_HDEEM_SYNC_PLUGIN_CONNECTION="INBAND" 
export SCOREP_METRIC_HDEEM_SYNC_PLUGIN_VERBOSE="WARN"
export SCOREP_METRIC_HDEEM_SYNC_PLUGIN_STATS_TIMEOUT_MS=1000  #=1000[ERROR]: Error occoured: Overflow during hdeem measurements.
export SCOREP_MPI_ENABLE_GROUPS=ENV


#psc_frontend  --apprun="${ROOT_DIR}/../qe-6.1-scorep/PW/src/pw.x"  --mpinumprocs=24 --ompnumthreads=1 --phase=$PHASE_ --tune=readex_intraphase --config-file="${ROOT_DIR}/qe_scorep_no_inst.xml" --force-localhost --info=2000 --selective-info=AutotuneAll,AutotunePlugins #--timeout=60 #--strategy=Importance 

psc_frontend  --apprun="/home/portero/sw/quantum_esp/qe_readex_git/qe-6.1-scorep.Inst/PW/src/pw.x -i ZnO.in"  --mpinumprocs=24 --ompnumthreads=1 --phase=$PHASE_ --tune=readex_intraphase --config-file="/home/portero/sw/quantum_esp/qe_readex_git/input_data_small/qe_scorep_inst.zn.xml" --force-localhost --info=1 --selective-info=AutotuneAll,AutotunePlugins #--timeout=60 #--strategy=Importance 
echo "run PTF done."

