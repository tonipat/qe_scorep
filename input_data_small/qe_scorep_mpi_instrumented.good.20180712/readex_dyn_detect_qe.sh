#!/bin/bash

export PATH=$PATH:/home/portero/sw/readex-apps/readex-repository/env/intelmpi2017.2.174_intel2017.2.174:/sw/global/compilers/intel/2017/impi/2017.2.174/bin64

#. set_env_ptf.source
. set_env_ptf_hdeem.source

#Go to proper path: (i.e. scorep-<*data_and_time>)
readex-dyn-detect -t 0.001 -p runpwscf -c 1 -v 0.001 -w 1 -r scorep -f report_qe ./profile.cubex
