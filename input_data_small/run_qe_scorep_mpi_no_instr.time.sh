#!/bin/bash

#!/bin/bash -l
#SBATCH --nodes=1
#SBATCH --tasks-per-node=1
#SBATCH --cpus-per-task=24
#SBATCH --time=0-02:00:00
#SBATCH -p haswell
#SBATCH -A p_readex
#SBATCH --reservation=READEX
#SBATCH --exclusive
#SBATCH --mem=4000
#SBATCH --comment="qe_scorep_test"

# Possible solution IMPORT tonipat@20180619: https://software.intel.com/es-es/forums/intel-fortran-compiler-for-linux-and-mac-os-x/topic/737584
module --force purge
ml modenv/both
ml pdt/3.18.1
#ml intel/2017a <==It has GCC. NO!
ml fftw/3.3.5-intel-xmpi
ml mkl
#ml glib
#ml intel
#ml scorep
module load intelmpi/2017.2.174
#ml load x86_adapt--> this is missing tonipat@20180613
#ml intel

#ml scorep
#ml intel/2017.2.174
#ml OpenBLAS/0.2.20-GCC-6.4.0-2.28 <--GCC NO
#ml ScaLAPACK/2.0.2-gompic-2018a-OpenBLAS-0.2.20 <==GCC NO!
ml list 

#PW/src/input.f90 313
. /home/portero/sw/readex-apps/readex-repository/env/intelmpi2017.2.174_intel2017.2.174/set_env_rdd.source


export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/sw/global/compilers/intel/2017/mkl/lib/intel64:/usr/local/lib:/usr/local/lib:/projects/p_readex/scorep/ci_TRY_READEX_online_access_call_tree_extensions_intelmpi2017.2.174_intel2017.2.174/lib
#ml scorep-4
#export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/portero/sw/scorep-4.0-install/lib
#export PATH=$PATH:/home/portero/sw/scorep-4.0-install/include
export BIN_DIR=/home/portero/sw/quantum_esp/qe_readex_git/qe-6.1-scorep.noInst/bin
export BIN_DIR2=/home/portero/sw/quantum_esp/qe_readex_git/qe-6.1.native/PW/src

proc=24
rm time.out
#RUN native
echo "Native execution" >> time.out

start=`date +%s%N`
mpirun -n $proc $BIN_DIR/pw.x < ZnO.in 
end=`date +%s%N`
runtime=$((end-start))
echo "Native Runtime: $runtime"  >> time.out


export SCOREP_VERBOSE=true
export SCOREP_TOTAL_MEMORY=4000M
export SCOREP_EXPERIMENT_DIRECTORY="./qe_scorep_mpi_no_instrumented"
export SCOREP_OVERWRITE_EXPERIMENT_DIRECTORY=true
export SCOREP_PROFILING_FORMAT=cube_tuple
#export SCOREP_ONLINEACCESS_ENABLE=true
export SCOREP_ENABLE_TRACING=false
export SCOREP_ENABLE_PROFILING=true
export SCOREP_PROFILING_ENABLE_CORE_FILES=false
export SCOREP_TRACING_COMPRESS=false
export SCOREP_METRIC_PAPI=PAPI_TOT_INS,PAPI_L3_TCM
#export SCOREP_METRIC_PAPI=PAPI_TOT_INS
#export SCOREP_FILTERING_FILE='./qe_filter.filt'
export SCOREP_FILTERING_FILE=''
echo > scorep_scorep_mpi



export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/sw/global/compilers/intel/2017/mkl/lib/intel64:/usr/local/lib:/usr/local/lib:/projects/p_readex/scorep/ci_TRY_READEX_online_access_call_tree_extensions_intelmpi2017.2.174_intel2017.2.174/lib

#QE Variables
#export BIN_DIR=$PREFIX/bin




#proc=1
echo "Without Filter execution"  >> time.out

start=`date +%s%N`
mpirun -n $proc $BIN_DIR/pw.x < ZnO.in 
end=`date +%s%N`

runtime2=$((end-start))
echo "Without filter Runtime: $runtime2"  >> time.out

export SCOREP_VERBOSE=true
export SCOREP_TOTAL_MEMORY=4000M
export SCOREP_EXPERIMENT_DIRECTORY="./qe_scorep_mpi_no_instrumented"
export SCOREP_OVERWRITE_EXPERIMENT_DIRECTORY=true
export SCOREP_PROFILING_FORMAT=cube_tuple
#export SCOREP_ONLINEACCESS_ENABLE=true
export SCOREP_ENABLE_TRACING=false
export SCOREP_ENABLE_PROFILING=true
export SCOREP_PROFILING_ENABLE_CORE_FILES=false
export SCOREP_TRACING_COMPRESS=false
export SCOREP_METRIC_PAPI=PAPI_TOT_INS,PAPI_L3_TCM
#export SCOREP_METRIC_PAPI=PAPI_TOT_INS
export SCOREP_FILTERING_FILE='./qe_filter.filt'
#export SCOREP_FILTERING_FILE=''
echo > scorep_scorep_mpi



export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/sw/global/compilers/intel/2017/mkl/lib/intel64:/usr/local/lib:/usr/local/lib:/projects/p_readex/scorep/ci_TRY_READEX_online_access_call_tree_extensions_intelmpi2017.2.174_intel2017.2.174/lib

#QE Variables
#export BIN_DIR=$PREFIX/bin
export BIN_DIR=/home/portero/sw/quantum_esp/qe_readex_git/qe-6.1-scorep.noInst/bin

proc=24
#proc=1
echo "With Filter execution"  >> time.out

start=`date +%s%N`
mpirun -n $proc $BIN_DIR/pw.x < ZnO.in 
end=`date +%s%N`

runtime3=$((end-start))

echo "With filter Runtime: $runtime3"  >> time.out
#overhead1=$($((runtime2 - runtime)/runtime))
#overhead2=$($((runtime3 - runtime)/runtime))
#overhead1= 'scale=8; (runtime2-runtime)/runtime' | bc

calc() { awk "BEGIN{print $*}"; }
calc $(((runtime2-runtime)/runtime))
echo "Overhead Native vs. Filtered (in % 0-1.0):" >>time.out
awk "BEGIN{print ($runtime2-$runtime)/$runtime}" >> time.out
echo "overhead non filter (in % 0-1.0):" >>time.out
awk "BEGIN{print ($runtime3-$runtime)/$runtime}" >>time.out



