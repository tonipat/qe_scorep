#!/bin/bash

#SBATCH --time=24:00:00#walltime 
#SBATCH --nodes=1 #numberofnodesrequested;1forPTFandremainingforapplicationrun 
#SBATCH --tasks-per-node=1 #numberofprocessespernodeforapplicationrun 
#SBATCH --cpus-per-task=24
#SBATCH --exclusive 
#SBATCH --partition=haswell 
#SBATCH --mem-per-cpu=2500M #memoryperCPUcore 10
#SBATCH -J "QE_PW_ScoreP" #jobname 
#SBATCH -A p_readex

module --force purge
ml modenv/both
ml pdt/3.18.1
ml load intel/2017a
module load intelmpi/2017.2.174

ml OpenBLAS/0.2.20-GCC-6.4.0-2.28
ml ScaLAPACK/2.0.2-gompic-2018a-OpenBLAS-0.2.20
ml scorep
ml list 



. /home/portero/sw/readex-apps/readex-repository/env/intelmpi2017.2.174_intel2017.2.174/set_env_saf.source



export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/sw/global/compilers/intel/2017/mkl/lib/intel64:projects/p_readex/it4i/meric/lib:/projects/p_readex/it4i/meric/hdeem:/usr/local/lib:/sw/taurus/libraries/papi/5.4.3/lib:/usr/local/lib:/projects/p_readex/scorep/ci_TRY_READEX_online_access_call_tree_extensions_intelmpi2017.2.174_intel2017.2.174/lib



cp ../config/make.def.ptf_manual config/make.def #????


export SCOREP_VERBOSE=true
export SCOREP_TOTAL_MEMORY=4000M
export SCOREP_EXPERIMENT_DIRECTORY="./qe_small_dataset_scorep"
export SCOREP_OVERWRITE_EXPERIMENT_DIRECTORY=true
export SCOREP_PROFILING_FORMAT=cube_tuple
#export SCOREP_ONLINEACCESS_ENABLE=true
export SCOREP_ENABLE_TRACING=false
export SCOREP_ENABLE_PROFILING=true
export SCOREP_PROFILING_ENABLE_CORE_FILES=false
export SCOREP_TRACING_COMPRESS=false
export SCOREP_METRIC_PAPI=PAPI_TOT_INS,PAPI_L3_TCM
#export SCOREP_METRIC_PAPI=PAPI_TOT_INS
export SCOREP_FILTERING_FILE='qe_small_data_set_scorep'
echo > qe_small_data_set_scorep


export PREFIX=/home/portero/sw/quantum_esp/qe_readex_master/qe-6.1-v3
export BIN_DIR=$PREFIX/bin
export PSEUDO_DIR=$PREFIX/pseudo

export OMP_NUM_THREADS=2

mpirun -n 1 $BIN_DIR/pw.x < pw.in # > pw.out



