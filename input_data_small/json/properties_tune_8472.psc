<?xml version="1.0" encoding="UTF-8"?>
<Experiment xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.lrr.in.tum.de/Periscope" xsi:schemaLocation="http://www.lrr.in.tum.de/Periscope psc_properties.xsd ">

  <date>2018-07-10</date>
  <time>20:27:00</time>
  <numProcs>1</numProcs>
  <numThreads>1</numThreads>
  <dir>/home/portero/sw/quantum_esp/qe_readex_git/input_data_small</dir>

  <property cluster="false" ID="131-0" >
	<name>Energy Consumption</name>
	<context FileID="5" FileName="psc_file_name_none" RFL="86" Config="1x1" RegionType="27" RegionId="psc_file_name_none*mainpwscf*86" >
		<execObj process="0" thread="0"/>
	</context>
	<severity>1.94151e+07</severity>
	<confidence>1</confidence>
	<purpose>100</purpose>
	<addInfo>
		<CPUEnergy>6828218</CPUEnergy>
		<ExecTime>188.582</ExecTime>
		<Instances>1</Instances>
		<NodeEnergy>1.94151e+07</NodeEnergy>
		<ScenarioID>0</ScenarioID>
	</addInfo>
  </property>
  <property cluster="false" ID="131-0" >
	<name>Energy Consumption</name>
	<context FileID="5" FileName="psc_file_name_none" RFL="497" Config="1x1" RegionType="7" RegionId="psc_file_name_none*hpsiinvlocpsik*497" >
		<execObj process="0" thread="0"/>
	</context>
	<severity>244166</severity>
	<confidence>1</confidence>
	<purpose>99</purpose>
	<addInfo>
		<CPUEnergy>85845</CPUEnergy>
		<ExecTime>2.05229</ExecTime>
		<Instances>36</Instances>
		<NodeEnergy>244166</NodeEnergy>
		<ScenarioID>0</ScenarioID>
	</addInfo>
  </property>
  <property cluster="false" ID="131-0" >
	<name>Energy Consumption</name>
	<context FileID="5" FileName="psc_file_name_none" RFL="497" Config="1x1" RegionType="7" RegionId="psc_file_name_none*hpsiinvlocpsik*497" >
		<execObj process="0" thread="0"/>
	</context>
	<severity>7.85382e+06</severity>
	<confidence>1</confidence>
	<purpose>99</purpose>
	<addInfo>
		<CPUEnergy>2778148</CPUEnergy>
		<ExecTime>70.2011</ExecTime>
		<Instances>1000</Instances>
		<NodeEnergy>7.85382e+06</NodeEnergy>
		<ScenarioID>0</ScenarioID>
	</addInfo>
  </property>
  <property cluster="false" ID="131-0" >
	<name>Energy Consumption</name>
	<context FileID="5" FileName="psc_file_name_none" RFL="86" Config="1x1" RegionType="27" RegionId="psc_file_name_none*mainpwscf*86" >
		<execObj process="0" thread="0"/>
	</context>
	<severity>2.044e+07</severity>
	<confidence>1</confidence>
	<purpose>100</purpose>
	<addInfo>
		<CPUEnergy>8228869</CPUEnergy>
		<ExecTime>181.472</ExecTime>
		<Instances>1</Instances>
		<NodeEnergy>2.044e+07</NodeEnergy>
		<ScenarioID>1</ScenarioID>
	</addInfo>
  </property>
  <property cluster="false" ID="131-0" >
	<name>Energy Consumption</name>
	<context FileID="5" FileName="psc_file_name_none" RFL="497" Config="1x1" RegionType="7" RegionId="psc_file_name_none*hpsiinvlocpsik*497" >
		<execObj process="0" thread="0"/>
	</context>
	<severity>247555</severity>
	<confidence>1</confidence>
	<purpose>99</purpose>
	<addInfo>
		<CPUEnergy>100483</CPUEnergy>
		<ExecTime>2.03388</ExecTime>
		<Instances>36</Instances>
		<NodeEnergy>247555</NodeEnergy>
		<ScenarioID>1</ScenarioID>
	</addInfo>
  </property>
  <property cluster="false" ID="131-0" >
	<name>Energy Consumption</name>
	<context FileID="5" FileName="psc_file_name_none" RFL="497" Config="1x1" RegionType="7" RegionId="psc_file_name_none*hpsiinvlocpsik*497" >
		<execObj process="0" thread="0"/>
	</context>
	<severity>8.27331e+06</severity>
	<confidence>1</confidence>
	<purpose>99</purpose>
	<addInfo>
		<CPUEnergy>3355428</CPUEnergy>
		<ExecTime>67.5855</ExecTime>
		<Instances>1000</Instances>
		<NodeEnergy>8.27331e+06</NodeEnergy>
		<ScenarioID>1</ScenarioID>
	</addInfo>
  </property>
  <property cluster="false" ID="131-0" >
	<name>Energy Consumption</name>
	<context FileID="5" FileName="psc_file_name_none" RFL="86" Config="1x1" RegionType="27" RegionId="psc_file_name_none*mainpwscf*86" >
		<execObj process="0" thread="0"/>
	</context>
	<severity>2.39209e+07</severity>
	<confidence>1</confidence>
	<purpose>100</purpose>
	<addInfo>
		<CPUEnergy>11563979</CPUEnergy>
		<ExecTime>179.234</ExecTime>
		<Instances>1</Instances>
		<NodeEnergy>2.39209e+07</NodeEnergy>
		<ScenarioID>2</ScenarioID>
	</addInfo>
  </property>
  <property cluster="false" ID="131-0" >
	<name>Energy Consumption</name>
	<context FileID="5" FileName="psc_file_name_none" RFL="497" Config="1x1" RegionType="7" RegionId="psc_file_name_none*hpsiinvlocpsik*497" >
		<execObj process="0" thread="0"/>
	</context>
	<severity>253618</severity>
	<confidence>1</confidence>
	<purpose>99</purpose>
	<addInfo>
		<CPUEnergy>127149</CPUEnergy>
		<ExecTime>2.03646</ExecTime>
		<Instances>36</Instances>
		<NodeEnergy>253618</NodeEnergy>
		<ScenarioID>2</ScenarioID>
	</addInfo>
  </property>
  <property cluster="false" ID="131-0" >
	<name>Energy Consumption</name>
	<context FileID="5" FileName="psc_file_name_none" RFL="497" Config="1x1" RegionType="7" RegionId="psc_file_name_none*hpsiinvlocpsik*497" >
		<execObj process="0" thread="0"/>
	</context>
	<severity>9.8107e+06</severity>
	<confidence>1</confidence>
	<purpose>99</purpose>
	<addInfo>
		<CPUEnergy>4790000</CPUEnergy>
		<ExecTime>66.9878</ExecTime>
		<Instances>1000</Instances>
		<NodeEnergy>9.8107e+06</NodeEnergy>
		<ScenarioID>2</ScenarioID>
	</addInfo>
  </property>
  <property cluster="false" ID="131-0" >
	<name>Energy Consumption</name>
	<context FileID="5" FileName="psc_file_name_none" RFL="86" Config="1x1" RegionType="27" RegionId="psc_file_name_none*mainpwscf*86" >
		<execObj process="0" thread="0"/>
	</context>
	<severity>1.52299e+07</severity>
	<confidence>1</confidence>
	<purpose>100</purpose>
	<addInfo>
		<CPUEnergy>5461886</CPUEnergy>
		<ExecTime>146.401</ExecTime>
		<Instances>1</Instances>
		<NodeEnergy>1.52299e+07</NodeEnergy>
		<ScenarioID>3</ScenarioID>
	</addInfo>
  </property>
  <property cluster="false" ID="131-0" >
	<name>Energy Consumption</name>
	<context FileID="5" FileName="psc_file_name_none" RFL="497" Config="1x1" RegionType="7" RegionId="psc_file_name_none*hpsiinvlocpsik*497" >
		<execObj process="0" thread="0"/>
	</context>
	<severity>323902</severity>
	<confidence>1</confidence>
	<purpose>99</purpose>
	<addInfo>
		<CPUEnergy>118953</CPUEnergy>
		<ExecTime>2.85026</ExecTime>
		<Instances>36</Instances>
		<NodeEnergy>323902</NodeEnergy>
		<ScenarioID>3</ScenarioID>
	</addInfo>
  </property>
  <property cluster="false" ID="131-0" >
	<name>Energy Consumption</name>
	<context FileID="5" FileName="psc_file_name_none" RFL="497" Config="1x1" RegionType="7" RegionId="psc_file_name_none*hpsiinvlocpsik*497" >
		<execObj process="0" thread="0"/>
	</context>
	<severity>6.00794e+06</severity>
	<confidence>1</confidence>
	<purpose>99</purpose>
	<addInfo>
		<CPUEnergy>2164140</CPUEnergy>
		<ExecTime>53.0332</ExecTime>
		<Instances>1000</Instances>
		<NodeEnergy>6.00794e+06</NodeEnergy>
		<ScenarioID>3</ScenarioID>
	</addInfo>
  </property>
  <property cluster="false" ID="131-0" >
	<name>Energy Consumption</name>
	<context FileID="5" FileName="psc_file_name_none" RFL="86" Config="1x1" RegionType="27" RegionId="psc_file_name_none*mainpwscf*86" >
		<execObj process="0" thread="0"/>
	</context>
	<severity>1.53922e+07</severity>
	<confidence>1</confidence>
	<purpose>100</purpose>
	<addInfo>
		<CPUEnergy>6287380</CPUEnergy>
		<ExecTime>135.084</ExecTime>
		<Instances>1</Instances>
		<NodeEnergy>1.53922e+07</NodeEnergy>
		<ScenarioID>4</ScenarioID>
	</addInfo>
  </property>
  <property cluster="false" ID="131-0" >
	<name>Energy Consumption</name>
	<context FileID="5" FileName="psc_file_name_none" RFL="497" Config="1x1" RegionType="7" RegionId="psc_file_name_none*hpsiinvlocpsik*497" >
		<execObj process="0" thread="0"/>
	</context>
	<severity>317045</severity>
	<confidence>1</confidence>
	<purpose>99</purpose>
	<addInfo>
		<CPUEnergy>128984</CPUEnergy>
		<ExecTime>2.61494</ExecTime>
		<Instances>36</Instances>
		<NodeEnergy>317045</NodeEnergy>
		<ScenarioID>4</ScenarioID>
	</addInfo>
  </property>
  <property cluster="false" ID="131-0" >
	<name>Energy Consumption</name>
	<context FileID="5" FileName="psc_file_name_none" RFL="497" Config="1x1" RegionType="7" RegionId="psc_file_name_none*hpsiinvlocpsik*497" >
		<execObj process="0" thread="0"/>
	</context>
	<severity>6.05163e+06</severity>
	<confidence>1</confidence>
	<purpose>99</purpose>
	<addInfo>
		<CPUEnergy>2488521</CPUEnergy>
		<ExecTime>48.5424</ExecTime>
		<Instances>1000</Instances>
		<NodeEnergy>6.05163e+06</NodeEnergy>
		<ScenarioID>4</ScenarioID>
	</addInfo>
  </property>
  <property cluster="false" ID="131-0" >
	<name>Energy Consumption</name>
	<context FileID="5" FileName="psc_file_name_none" RFL="86" Config="1x1" RegionType="27" RegionId="psc_file_name_none*mainpwscf*86" >
		<execObj process="0" thread="0"/>
	</context>
	<severity>1.49579e+07</severity>
	<confidence>1</confidence>
	<purpose>100</purpose>
	<addInfo>
		<CPUEnergy>5572774</CPUEnergy>
		<ExecTime>140.247</ExecTime>
		<Instances>1</Instances>
		<NodeEnergy>1.49579e+07</NodeEnergy>
		<ScenarioID>5</ScenarioID>
	</addInfo>
  </property>
  <property cluster="false" ID="131-0" >
	<name>Energy Consumption</name>
	<context FileID="5" FileName="psc_file_name_none" RFL="497" Config="1x1" RegionType="7" RegionId="psc_file_name_none*hpsiinvlocpsik*497" >
		<execObj process="0" thread="0"/>
	</context>
	<severity>309753</severity>
	<confidence>1</confidence>
	<purpose>99</purpose>
	<addInfo>
		<CPUEnergy>114608</CPUEnergy>
		<ExecTime>2.71949</ExecTime>
		<Instances>36</Instances>
		<NodeEnergy>309753</NodeEnergy>
		<ScenarioID>5</ScenarioID>
	</addInfo>
  </property>
  <property cluster="false" ID="131-0" >
	<name>Energy Consumption</name>
	<context FileID="5" FileName="psc_file_name_none" RFL="497" Config="1x1" RegionType="7" RegionId="psc_file_name_none*hpsiinvlocpsik*497" >
		<execObj process="0" thread="0"/>
	</context>
	<severity>5.8804e+06</severity>
	<confidence>1</confidence>
	<purpose>99</purpose>
	<addInfo>
		<CPUEnergy>2203479</CPUEnergy>
		<ExecTime>50.698</ExecTime>
		<Instances>1000</Instances>
		<NodeEnergy>5.8804e+06</NodeEnergy>
		<ScenarioID>5</ScenarioID>
	</addInfo>
  </property>
  <property cluster="false" ID="131-0" >
	<name>Energy Consumption</name>
	<context FileID="5" FileName="psc_file_name_none" RFL="86" Config="1x1" RegionType="27" RegionId="psc_file_name_none*mainpwscf*86" >
		<execObj process="0" thread="0"/>
	</context>
	<severity>1.28589e+07</severity>
	<confidence>1</confidence>
	<purpose>100</purpose>
	<addInfo>
		<CPUEnergy>4744139</CPUEnergy>
		<ExecTime>121.295</ExecTime>
		<Instances>1</Instances>
		<NodeEnergy>1.28589e+07</NodeEnergy>
		<ScenarioID>6</ScenarioID>
	</addInfo>
  </property>
  <property cluster="false" ID="131-0" >
	<name>Energy Consumption</name>
	<context FileID="5" FileName="psc_file_name_none" RFL="497" Config="1x1" RegionType="7" RegionId="psc_file_name_none*hpsiinvlocpsik*497" >
		<execObj process="0" thread="0"/>
	</context>
	<severity>291480</severity>
	<confidence>1</confidence>
	<purpose>99</purpose>
	<addInfo>
		<CPUEnergy>107480</CPUEnergy>
		<ExecTime>2.38435</ExecTime>
		<Instances>36</Instances>
		<NodeEnergy>291480</NodeEnergy>
		<ScenarioID>6</ScenarioID>
	</addInfo>
  </property>
  <property cluster="false" ID="131-0" >
	<name>Energy Consumption</name>
	<context FileID="5" FileName="psc_file_name_none" RFL="497" Config="1x1" RegionType="7" RegionId="psc_file_name_none*hpsiinvlocpsik*497" >
		<execObj process="0" thread="0"/>
	</context>
	<severity>5.42474e+06</severity>
	<confidence>1</confidence>
	<purpose>99</purpose>
	<addInfo>
		<CPUEnergy>2026671</CPUEnergy>
		<ExecTime>44.134</ExecTime>
		<Instances>1000</Instances>
		<NodeEnergy>5.42474e+06</NodeEnergy>
		<ScenarioID>6</ScenarioID>
	</addInfo>
  </property>
  <property cluster="false" ID="131-0" >
	<name>Energy Consumption</name>
	<context FileID="5" FileName="psc_file_name_none" RFL="86" Config="1x1" RegionType="27" RegionId="psc_file_name_none*mainpwscf*86" >
		<execObj process="0" thread="0"/>
	</context>
	<severity>1.25122e+07</severity>
	<confidence>1</confidence>
	<purpose>100</purpose>
	<addInfo>
		<CPUEnergy>5251880</CPUEnergy>
		<ExecTime>107.583</ExecTime>
		<Instances>1</Instances>
		<NodeEnergy>1.25122e+07</NodeEnergy>
		<ScenarioID>7</ScenarioID>
	</addInfo>
  </property>
  <property cluster="false" ID="131-0" >
	<name>Energy Consumption</name>
	<context FileID="5" FileName="psc_file_name_none" RFL="497" Config="1x1" RegionType="7" RegionId="psc_file_name_none*hpsiinvlocpsik*497" >
		<execObj process="0" thread="0"/>
	</context>
	<severity>290067</severity>
	<confidence>1</confidence>
	<purpose>99</purpose>
	<addInfo>
		<CPUEnergy>121900</CPUEnergy>
		<ExecTime>2.0857</ExecTime>
		<Instances>36</Instances>
		<NodeEnergy>290067</NodeEnergy>
		<ScenarioID>7</ScenarioID>
	</addInfo>
  </property>
  <property cluster="false" ID="131-0" >
	<name>Energy Consumption</name>
	<context FileID="5" FileName="psc_file_name_none" RFL="497" Config="1x1" RegionType="7" RegionId="psc_file_name_none*hpsiinvlocpsik*497" >
		<execObj process="0" thread="0"/>
	</context>
	<severity>5.32584e+06</severity>
	<confidence>1</confidence>
	<purpose>99</purpose>
	<addInfo>
		<CPUEnergy>2246882</CPUEnergy>
		<ExecTime>38.8223</ExecTime>
		<Instances>1000</Instances>
		<NodeEnergy>5.32584e+06</NodeEnergy>
		<ScenarioID>7</ScenarioID>
	</addInfo>
  </property>
  <property cluster="false" ID="131-0" >
	<name>Energy Consumption</name>
	<context FileID="5" FileName="psc_file_name_none" RFL="86" Config="1x1" RegionType="27" RegionId="psc_file_name_none*mainpwscf*86" >
		<execObj process="0" thread="0"/>
	</context>
	<severity>1.23813e+07</severity>
	<confidence>1</confidence>
	<purpose>100</purpose>
	<addInfo>
		<CPUEnergy>5062338</CPUEnergy>
		<ExecTime>108.588</ExecTime>
		<Instances>1</Instances>
		<NodeEnergy>1.23813e+07</NodeEnergy>
		<ScenarioID>8</ScenarioID>
	</addInfo>
  </property>
  <property cluster="false" ID="131-0" >
	<name>Energy Consumption</name>
	<context FileID="5" FileName="psc_file_name_none" RFL="497" Config="1x1" RegionType="7" RegionId="psc_file_name_none*hpsiinvlocpsik*497" >
		<execObj process="0" thread="0"/>
	</context>
	<severity>288515</severity>
	<confidence>1</confidence>
	<purpose>99</purpose>
	<addInfo>
		<CPUEnergy>119879</CPUEnergy>
		<ExecTime>2.10627</ExecTime>
		<Instances>36</Instances>
		<NodeEnergy>288515</NodeEnergy>
		<ScenarioID>8</ScenarioID>
	</addInfo>
  </property>
  <property cluster="false" ID="131-0" >
	<name>Energy Consumption</name>
	<context FileID="5" FileName="psc_file_name_none" RFL="497" Config="1x1" RegionType="7" RegionId="psc_file_name_none*hpsiinvlocpsik*497" >
		<execObj process="0" thread="0"/>
	</context>
	<severity>5.24726e+06</severity>
	<confidence>1</confidence>
	<purpose>99</purpose>
	<addInfo>
		<CPUEnergy>2141391</CPUEnergy>
		<ExecTime>39.2563</ExecTime>
		<Instances>1000</Instances>
		<NodeEnergy>5.24726e+06</NodeEnergy>
		<ScenarioID>8</ScenarioID>
	</addInfo>
  </property>
  <property cluster="false" ID="131-0" >
	<name>Energy Consumption</name>
	<context FileID="5" FileName="psc_file_name_none" RFL="86" Config="1x1" RegionType="27" RegionId="psc_file_name_none*mainpwscf*86" >
		<execObj process="0" thread="0"/>
	</context>
	<severity>1.33241e+07</severity>
	<confidence>1</confidence>
	<purpose>100</purpose>
	<addInfo>
		<CPUEnergy>6515590</CPUEnergy>
		<ExecTime>98.6382</ExecTime>
		<Instances>1</Instances>
		<NodeEnergy>1.33241e+07</NodeEnergy>
		<ScenarioID>9</ScenarioID>
	</addInfo>
  </property>
  <property cluster="false" ID="131-0" >
	<name>Energy Consumption</name>
	<context FileID="5" FileName="psc_file_name_none" RFL="497" Config="1x1" RegionType="7" RegionId="psc_file_name_none*hpsiinvlocpsik*497" >
		<execObj process="0" thread="0"/>
	</context>
	<severity>281472</severity>
	<confidence>1</confidence>
	<purpose>99</purpose>
	<addInfo>
		<CPUEnergy>143957</CPUEnergy>
		<ExecTime>1.77525</ExecTime>
		<Instances>36</Instances>
		<NodeEnergy>281472</NodeEnergy>
		<ScenarioID>9</ScenarioID>
	</addInfo>
  </property>
  <property cluster="false" ID="131-0" >
	<name>Energy Consumption</name>
	<context FileID="5" FileName="psc_file_name_none" RFL="497" Config="1x1" RegionType="7" RegionId="psc_file_name_none*hpsiinvlocpsik*497" >
		<execObj process="0" thread="0"/>
	</context>
	<severity>5.78506e+06</severity>
	<confidence>1</confidence>
	<purpose>99</purpose>
	<addInfo>
		<CPUEnergy>2855010</CPUEnergy>
		<ExecTime>36.7289</ExecTime>
		<Instances>1000</Instances>
		<NodeEnergy>5.78506e+06</NodeEnergy>
		<ScenarioID>9</ScenarioID>
	</addInfo>
  </property>
  <property cluster="false" ID="131-0" >
	<name>Energy Consumption</name>
	<context FileID="5" FileName="psc_file_name_none" RFL="86" Config="1x1" RegionType="27" RegionId="psc_file_name_none*mainpwscf*86" >
		<execObj process="0" thread="0"/>
	</context>
	<severity>1.33095e+07</severity>
	<confidence>1</confidence>
	<purpose>100</purpose>
	<addInfo>
		<CPUEnergy>6474839</CPUEnergy>
		<ExecTime>98.5689</ExecTime>
		<Instances>1</Instances>
		<NodeEnergy>1.33095e+07</NodeEnergy>
		<ScenarioID>10</ScenarioID>
	</addInfo>
  </property>
  <property cluster="false" ID="131-0" >
	<name>Energy Consumption</name>
	<context FileID="5" FileName="psc_file_name_none" RFL="497" Config="1x1" RegionType="7" RegionId="psc_file_name_none*hpsiinvlocpsik*497" >
		<execObj process="0" thread="0"/>
	</context>
	<severity>284193</severity>
	<confidence>1</confidence>
	<purpose>99</purpose>
	<addInfo>
		<CPUEnergy>141482</CPUEnergy>
		<ExecTime>1.77469</ExecTime>
		<Instances>36</Instances>
		<NodeEnergy>284193</NodeEnergy>
		<ScenarioID>10</ScenarioID>
	</addInfo>
  </property>
  <property cluster="false" ID="131-0" >
	<name>Energy Consumption</name>
	<context FileID="5" FileName="psc_file_name_none" RFL="497" Config="1x1" RegionType="7" RegionId="psc_file_name_none*hpsiinvlocpsik*497" >
		<execObj process="0" thread="0"/>
	</context>
	<severity>5.8374e+06</severity>
	<confidence>1</confidence>
	<purpose>99</purpose>
	<addInfo>
		<CPUEnergy>2856802</CPUEnergy>
		<ExecTime>36.7243</ExecTime>
		<Instances>1000</Instances>
		<NodeEnergy>5.8374e+06</NodeEnergy>
		<ScenarioID>10</ScenarioID>
	</addInfo>
  </property>
  <property cluster="false" ID="131-0" >
	<name>Energy Consumption</name>
	<context FileID="5" FileName="psc_file_name_none" RFL="86" Config="1x1" RegionType="27" RegionId="psc_file_name_none*mainpwscf*86" >
		<execObj process="0" thread="0"/>
	</context>
	<severity>1.33168e+07</severity>
	<confidence>1</confidence>
	<purpose>100</purpose>
	<addInfo>
		<CPUEnergy>6487882</CPUEnergy>
		<ExecTime>98.7141</ExecTime>
		<Instances>1</Instances>
		<NodeEnergy>1.33168e+07</NodeEnergy>
		<ScenarioID>11</ScenarioID>
	</addInfo>
  </property>
  <property cluster="false" ID="131-0" >
	<name>Energy Consumption</name>
	<context FileID="5" FileName="psc_file_name_none" RFL="497" Config="1x1" RegionType="7" RegionId="psc_file_name_none*hpsiinvlocpsik*497" >
		<execObj process="0" thread="0"/>
	</context>
	<severity>270839</severity>
	<confidence>1</confidence>
	<purpose>99</purpose>
	<addInfo>
		<CPUEnergy>134223</CPUEnergy>
		<ExecTime>1.77466</ExecTime>
		<Instances>36</Instances>
		<NodeEnergy>270839</NodeEnergy>
		<ScenarioID>11</ScenarioID>
	</addInfo>
  </property>
  <property cluster="false" ID="131-0" >
	<name>Energy Consumption</name>
	<context FileID="5" FileName="psc_file_name_none" RFL="497" Config="1x1" RegionType="7" RegionId="psc_file_name_none*hpsiinvlocpsik*497" >
		<execObj process="0" thread="0"/>
	</context>
	<severity>5.74013e+06</severity>
	<confidence>1</confidence>
	<purpose>99</purpose>
	<addInfo>
		<CPUEnergy>2823089</CPUEnergy>
		<ExecTime>36.7528</ExecTime>
		<Instances>1000</Instances>
		<NodeEnergy>5.74013e+06</NodeEnergy>
		<ScenarioID>11</ScenarioID>
	</addInfo>
  </property>
</Experiment>
