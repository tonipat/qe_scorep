#!/bin/bash

#SBATCH --time=24:00:00#walltime 
#SBATCH --nodes=1 #numberofnodesrequested;1forPTFandremainingforapplicationrun 
#SBATCH --tasks-per-node=1 #numberofprocessespernodeforapplicationrun 
#SBATCH --cpus-per-task=24
#SBATCH --exclusive 
#SBATCH --partition=haswell 
#SBATCH --mem-per-cpu=2500M #memoryperCPUcore 10
#SBATCH -J "QE_PW_ScoreP" #jobname 
#SBATCH -A p_readex

module --force purge


#aml load OpenMPI/2.1.1-GCC-6.3.0-2.27

#ml load imkl/11.2.3.187
ml  modenv/both
#ml load FFTW/3.3.6-gompi-2017a
ml FFTW/3.3.7-intel-2017b # tonipat@20180614,Intel instead of GCC
#ml load LAPACK/3.7.1-gompi-2017a
ml OpenBLAS/0.2.20-GCC-6.4.0-2.28
ml ScaLAPACK/2.0.2-gompic-2018a-OpenBLAS-0.2.20

module load intelmpi/2017.2.174
ml intel/2018.1.163
ml list 
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/sw/generic/imkl/2018.1.163-iimpi-2018a/compilers_and_libraries_2018.1.163/linux/mkl/lib/intel64_lin:/sw/generic/imkl/2018.1.163-iompi-2018a/compilers_and_libraries_2018.1.163/linux/mkl/lib/intel64_lin

ROOT_DIR=${PWD}
export PATH=$PATH:${ROOT_DIR}/../env/intelmpi2017.2.174_intel2017.2.174:/sw/global/compilers/intel/2017/impi/2017.2.174/bin64

. set_env_saf.source


export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib  
export SCOREP_SUBSTRATE_PLUGINS=rrl 
export SCOREP_RRL_PLUGINS=cpu_freq_plugin,uncore_freq_plugin
export SCOREP_RRL_VERBOSE="WARN" 
module load scorep-hdeem/sync-hdeem2.2.5-intelmpi-intel2017 #sync-xmpi-gcc6.3 #intel Ondrej question? (find equivalent)???
export SCOREP_METRIC_PLUGINS=hdeem_sync_plugin
export SCOREP_METRIC_PLUGINS_SEP=";" 
export SCOREP_METRIC_HDEEM_SYNC_PLUGIN="*/E"
export SCOREP_METRIC_HDEEM_SYNC_PLUGIN_CONNECTION="INBAND" 
export SCOREP_METRIC_HDEEM_SYNC_PLUGIN_VERBOSE="WARN"
export SCOREP_METRIC_HDEEM_SYNC_PLUGIN_STATS_TIMEOUT_MS=1000  
export SCOREP_MPI_ENABLE_GROUPS=ENV 


export PREFIX=/home/portero/sw/quantum_esp/qe_readex_master/qe-6.1-v3
export BIN_DIR=$PREFIX/bin
export PSEUDO_DIR=$PREFIX/pseudo

export OMP_NUM_THREADS=2
psc_frontend  --apprun=" ${ROOT_DIR}../qe-6.1-v3/bin/pw.x"  --mpinumprocs=1 --ompnumthreads=$OMP_NUM_THREADS --phase=$PHASE_ --tune=readex_intraphase --config-file="${ROOT_DIR}/scripts/XXX.missing!" --force-localhost --info=2000 --selective-info=AutotuneAll,AutotunePlugins #--timeout=60 #--strategy=Importance 
echo "run PTF done."
mpirun -n 12 $BIN_DIR/pw.x < pw.in # > pw.out



