#!/bin/bash

export PATH=$PATH:/home/portero/sw/readex-apps/readex-repository/env/intelmpi2017.2.174_intel2017.2.174:/sw/global/compilers/intel/2017/impi/2017.2.174/bin64

#. set_env_ptf.source
. set_env_rdd.source

PHASE=$1

#Go to proper path: (i.e. scorep-<*data_and_time>)
#eadex-dyn-detect -t 0.001 -p mainpwscf -c 1 -v 0.001 -w 1 -r scorep -f report_qe ./profile.cubex

readex-dyn-detect -t 0.001 -p $PHASE -c 0.001 -v 10 -w 1 -r scorep -f report_qe ./profile.cubex
