#!/bin/bash

#SBATCH --time=1:00:00 # walltime
#SBATCH --nodes=1 # number of processor cores (i.e. tasks)
#SBATCH --ntasks-per-node=24
#SBATCH --cpus-per-task=1
#SBATCH --exclusive
#SBATCH --partition=haswell
#SBATCH --comment="no_monitoring"
#SBATCH --mem-per-cpu=2500M # memory per CPU core
#SBATCH -A p_readex
##SBATCH --reservation=READEX
#SBATCH -J "qe_saf" # job name
#SBATCH --output=qe_saf.out
#SBATCH --error=qe_saf.out
###############################################################################

module purge
source ./readex_env/set_env_saf.source
#source ./set_env_qe.source
export SCOREP_FILTERING_FILE=${PWD}/scorep.filt
cd input_data_small

export SCOREP_TOTAL_MEMORY=4000M


rm -rf scorep-*
rm -f old_scorep.filt
echo "" > scorep.filt

if [ "$READEX_INTEL" == "1" ]; then
  rm -rf old_scorep_icc.filt
  echo "" > scorep_icc.filt
fi

#SAF_t=0.001 #1 ms
#SAF_t=0.01 #10 ms
SAF_t=0.1 #100 ms

result=1
while [ $result != 0 ]; do
  echo "result = "$result
  srun --cpu_bind=verbose,cores --nodes 1 --ntasks-per-node 24 --cpus-per-task 1 -n 24 ../qe-6.1-scorep.noInst/PW/src/pw.x < pw.in 
  echo "Aplication run - done."
  ../do_scorep_autofilter_single.sh $SAF_t
  result=$?
 
if [ "$READEX_INTEL" == "1" ] && [ $result != 0 ]; then
  export FILTER_INTEL="-tcollect-filter=scorep_icc.filt"
  ./compile_for_saf.sh
fi

  echo "scorep_autofilter_single done ($result)."
done
 cp scorep*.filt ../.

echo "end of scorep-autofilter."
