#!/bin/bash
# Possible solution IMPORT tonipat@20180619: https://software.intel.com/es-es/forums/intel-fortran-compiler-for-linux-and-mac-os-x/topic/737584
module --force purge
ml modenv/both

. /home/portero/sw/readex-apps/readex-repository/env/intelmpi2017.2.174_intel2017.2.174/set_env_saf.source

ml pdt/3.18.1
#ml intel/2017a <==It has GCC. NO!
ml fftw/3.3.5-intel-xmpi
#ml mkl
#ml FFTW/3.3.7-intel-2017b
module load intelmpi/2017.2.174
#ml load x86_adapt--> this is missing tonipat@20180613
#ml intel/2017a
ml ScaLAPACK/2.0.2-gompi-2018a-OpenBLAS-0.2.20 #blas is needed but I do not see only intel
#ml scorep
#ml glib
#ml intel/2017.2.174
#ml OpenBLAS/0.2.20-GCC-6.4.0-2.28 #<--GCC NO
#ml ScaLAPACK/2.0.2-gompic-2018a-OpenBLAS-0.2.20 #<==GCC NO!
ml scorep/ci_TRY_READEX_online_access_call_tree_extensions_intelmpi2017.2.174_intel2017.2.174
ml list 

#PW/src/input.f90 313



export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/sw/global/compilers/intel/2017/mkl/lib/intel64:projects/p_readex/it4i/meric/lib:/projects/p_readex/it4i/meric/hdeem:/usr/local/lib:/sw/taurus/libraries/papi/5.4.3/lib:/usr/local/lib:/projects/p_readex/scorep/ci_TRY_READEX_online_access_call_tree_extensions_intelmpi2017.2.174_intel2017.2.174/lib

#export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/sw/global/compilers/intel/2017/mkl/lib/intel64:/usr/local/lib:/usr/local/lib:/projects/p_readex/scorep/ci_TRY_READEX_online_access_call_tree_extensions_intelmpi2017.2.174_intel2017.2.174/lib
#ml scorep-4
#export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/portero/sw/scorep-4.0-install/lib
#export PATH=$PATH:/home/portero/sw/scorep-4.0-install/include

#Manual
#export CC='scorep --mpp=mpi --online-access  --user --thread=omp --nocompiler mpiicc -qopenmp -L/usr/local/lib -lx86_adapt -L/sw/taurus/libraries/papi/5.5.1/lib -lpapi -lcpufreq  -lfreeipmi -lhdeem'

#export CXX='scorep --mpp=mpi --online-access --user --thread=omp --nocompiler mpiicpc -qopenmp -L/usr/local/lib -lx86_adapt -L/sw/taurus/libraries/papi/5.5.1/lib -lpapi -lcpufreq  -lfreeipmi  -lhdeem'
#Instrumented

#Instrumented
#export CC='scorep --mpp=mpi --online-access  --user --thread=omp  mpiicc -qopenmp -L/usr/local/lib -lx86_adapt -L/sw/taurus/libraries/papi/5.5.1/lib -lpapi -lcpufreq  -lfreeipmi -lhdeem'

#export CXX='scorep --mpp=mpi --online-access --user --thread=omp  mpiicpc -qopenmp -L/usr/local/lib -lx86_adapt -L/sw/taurus/libraries/papi/5.5.1/lib -lpapi -lcpufreq  -lfreeipmi  -lhdeem'


export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/portero/sw/quantum_esp/qe_readex_master/qe-6.1-scorep.Inst/qe_libs

cp config/make.def.ptf_manual config/make.def #???? Unnecessary like this 

cd qe-6.1-scorep.Inst
cp make.inc.instrumented make.inc
#make distclean

#export FFLAGS="-DHAVE_MERIC" 
#export LD_LIBS="-lmeric -L/home/lriha/meric/lib/ -lx86_adapt"
#export IFLAGS="-I/home/lriha/meric/include"


# ./configure --enable-openmp --enable-parallel

#echo "IFLAGS         += -I/home/lriha/meric/include" >> make.inc --> Equivalent: ml scorep/...

#echo "FFLAGS         += -DHAVE_MERIC" >> make.inc
#echo "FFLAGS         += -DWIDTH_SCOREP" >> make.inc
#echo "LDFLAGS        += scorep --online-access --user --nocompiler mpif77 -fopenmp">>make.inc
#ml scorep/3.0-intel-xmpi-cuda8.0

#make clean 

make pw -j 22

#make pw -j 22

#make all -j 22
#make all 

# output is in the bin directory 


