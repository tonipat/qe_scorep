#!/bin/bash

module purge
source ./readex_env/set_env_rdd.source
#source ./set_env_qe.source

if [ "$READEX_INTEL" == "1" ]; then
  export FILTER_GNU=""
  export FILTER_INTEL="-tcollect-filter=${PWD}/scorep_icc.filt"
else
  export FILTER_GNU="--instrument-filter=${PWD}/scorep.filt"
  export FILTER_INTEL=""
fi

cd qe-6.1-scorep.noInst

cp make.rdd.inc make.inc

#make clean 
make pw -j 22
