#!/bin/sh

#SBATCH --time=5:00:00#walltime 
#SBATCH --nodes=1 #numberofnodesrequested;1forPTFandremainingforapplicationrun 
#SBATCH --tasks-per-node=1 #numberofprocessespernodeforapplicationrun 
#SBATCH --cpus-per-task=24
#SBATCH --exclusive 
#SBATCH --partition=haswell 
#SBATCH --mem-per-cpu=2500M #memoryperCPUcore 10
#SBATCH -J "BRRL_qe_inst" #jobname 
#SBATCH -A p_readex
#SBATCH --comment="cpufreqchown"
#SBATCH --output=qe_rrl.out
#SBATCH --error=qe_rrl.out

module purge
source ./readex_env/set_env_rrl.source

NP=1
PHASE_=mainpwcsf


echo "run RRL begin."

echo "Start plain run"

export SCOREP_ENABLE_PROFILING="false" 
export SCOREP_ENABLE_TRACING="false" 
export SCOREP_SUBSTRATE_PLUGINS="" 
export SCOREP_RRL_PLUGINS="" 
export SCOREP_RRL_TMM_PATH="" 
export SCOREP_MPI_ENABLE_GROUPS=ENV

clearHdeem
ROOT_DIR=${PWD}
cd input_data_small
rm hdeem_qe_instrumented.out
echo "Plain Run" >> hdeem_qe_instrumented.out
startHdeem
#mpirun -n 24 ${PWD}/../qe-6.1-scorep.noInst/PW/src/pw.x -i pw.in 
srun -N 1 -n 1 -c 24 ${PWD}/../qe-6.1-scorep.noInst/PW/src/pw.x -i pw.in 

checkHdeem >> hdeem_qe_instrumented.out


echo "Start RRL-tuned run"
export SCOREP_SUBSTRATE_PLUGINS='rrl'
export SCOREP_RRL_VERBOSE="WARN"
export SCOREP_RRL_PLUGINS=cpu_freq_plugin,uncore_freq_plugin
export SCOREP_RRL_TMM_PATH=${ROOT_DIR}/tuning_model.json
export SCOREP_ENABLE_TRACING=false
export SCOREP_ENABLE_PROFILING=false
export SCOREP_ENV_ENABLE_GROUPS=ENV
clearHdeem
echo "RRL-runed Run (1)" >> hdeem_qe_instrumented.out
startHdeem

#mpirun -n 24 ${PWD}/../qe-6.1-scorep.noInst/PW/src/pw.x -i pw.in 
#srun -N 1 -n 1 -c 24 ${PWD}/../qe-6.1-scorep.noInst/PW/src/pw.x -i pw.in
srun -N 1 -n 24  ${PWD}/../qe-6.1-scorep.noInst/PW/src/pw.x -i pw.in 
stopHdeem
checkHdeem >> hdeem_qe_instrumented.out
echo "RRL-runed Run (2)" >> hdeem_qe_instrumented.out
clearHdeem
startHdeem

#mpirun -n 24 ${PWD}/../qe-6.1-scorep.noInst/PW/src/pw.x -i pw.in 
#srun -N 1 -n 1 -c 24 ${PWD}/../qe-6.1-scorep.noInst/PW/src/pw.x -i pw.in
srun -N 1 -n 24 ${PWD}/../qe-6.1-scorep.noInst/PW/src/pw.x -i pw.in 

stopHdeem
checkHdeem >> hdeem_qe_instrumented.out
cp hdeem_qe_instrumented.out ../.
echo "run RRL done."
