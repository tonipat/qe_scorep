module purge
source ./readex_env/set_env_meric.source

export SCOREP_PREP=""
export DSWITCH="-DUSE_MERIC"

export PPROF_PATH="/home/portero/sw/gperftools/install/lib/libprofiler.so"

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/portero/sw/gperftools/install/lib
cd qe-6.1-meric
#cp make.inc.ori make.meric.inc

#echo "IFLAGS         += ${MERIC_INC_PATH} ${READEX_INC_PATH} " >> make.meric.inc
#echo "FFLAGS         += -DHAVE_MERIC" >> make.inc
#echo "LDFLAGS        += -lmericmpi -L/home/lriha/meric/lib/ -lx86_adapt" >> make.inc
#echo "LDFLAGS        += ${MERIC_LIBS} ${MERIC_LIB_PATH} ${READEX_OMP_FLAG}" >> make.meric.inc

cp make.meric_pprof.inc make.inc

make clean 

make pw -j 22
make pw 
