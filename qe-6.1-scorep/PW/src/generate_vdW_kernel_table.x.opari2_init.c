

#ifdef __cplusplus
extern "C"
{
#endif
#include <stddef.h>

extern void pomp2_init_reg_p45a743bfle4_12_();
extern void pomp2_init_reg_4cwkf643bmjv3_1_();
extern void pomp2_init_reg_tltcg643b6q78_1_();
extern void pomp2_init_reg_o45a743b9uk1_54_();
extern void pomp2_init_reg_rlc4c743b1ik6_93_();
extern void pomp2_init_reg_7inxk843btv69_4_();
extern void pomp2_init_reg_xv3sg443bo5ua_9_();
extern void pomp2_init_reg_fwoxy643bmtwc_3_();
extern void pomp2_init_reg_c96i4743b8odd_2_();
extern void pomp2_init_reg_rft4p543b8nwj_12_();
extern void pomp2_init_reg_h96i4743bhkgf_2_();
extern void pomp2_init_reg_84xa36243bwcz2_1_();
extern void pomp2_init_reg_h3kzav243bang7_2_();
extern void pomp2_init_reg_b96i4743bnhda_8_();
extern void pomp2_init_reg_xtcp9643bxbpc_3_();
extern void pomp2_init_reg_2kew1f343bl0vh_10_();
extern void pomp2_init_reg_idnhgl543bejba_1_();
extern void pomp2_init_reg_zwh0443bno0b_4_();
extern void pomp2_init_reg_e96i4743bg1hf_4_();
extern void pomp2_init_reg_7i10743b7eka_3_();
extern void pomp2_init_reg_wegpx543blobe_5_();

void POMP2_Init_regions()
{
    pomp2_init_reg_xtcp9643bxbpc_3_();
    pomp2_init_reg_idnhgl543bejba_1_();
    pomp2_init_reg_e96i4743bg1hf_4_();
    pomp2_init_reg_wegpx543blobe_5_();
    pomp2_init_reg_4cwkf643bmjv3_1_();
    pomp2_init_reg_o45a743b9uk1_54_();
    pomp2_init_reg_7inxk843btv69_4_();
    pomp2_init_reg_xv3sg443bo5ua_9_();
    pomp2_init_reg_fwoxy643bmtwc_3_();
    pomp2_init_reg_c96i4743b8odd_2_();
    pomp2_init_reg_rft4p543b8nwj_12_();
    pomp2_init_reg_84xa36243bwcz2_1_();
    pomp2_init_reg_h3kzav243bang7_2_();
    pomp2_init_reg_b96i4743bnhda_8_();
    pomp2_init_reg_2kew1f343bl0vh_10_();
    pomp2_init_reg_zwh0443bno0b_4_();
    pomp2_init_reg_7i10743b7eka_3_();
    pomp2_init_reg_p45a743bfle4_12_();
    pomp2_init_reg_tltcg643b6q78_1_();
    pomp2_init_reg_rlc4c743b1ik6_93_();
    pomp2_init_reg_h96i4743bhkgf_2_();
}

size_t POMP2_Get_num_regions()
{
    return 234;
}


void POMP2_USER_Init_regions()
{
}

size_t POMP2_USER_Get_num_regions()
{
    return 0;
}

#ifdef __cplusplus
}
#endif
