#!/bin/bash

module purge
source ./readex_env/set_env_ptf_hdeem.source
source ./set_env_qe.source

if [ "$READEX_INTEL" == "1" ]; then
  export FILTER_GNU=""
  export FILTER_INTEL="-tcollect-filter=/home/portero/sw/quantum_esp/qe_readex_git/scorep_icc.filt"
else
  export FILTER_GNU="--instrument-filter=/home/portero/sw/quantum_esp/qe_readex_git/scorep.filt"
  export FILTER_INTEL=""
fi

#export SCOREP_PREP="scorep --online-access --user --thread=omp --noopenmp "$FILTER_GNU
#export DSWITCH="-DUSE_SCOREP"

cd qe-6.1-scorep.noInst
#make clean
cp make.no_inst.inc make.inc
#cp make.ptf.inc make.inc


make clean 
make pw -j 22

