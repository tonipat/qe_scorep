#!/bin/sh

#SBATCH --time=16:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --tasks-per-node=24
#SBATCH --cpus-per-task=1
#SBATCH --exclusive
#SBATCH --partition=haswell
#SBATCH --comment="no_monitoring"
#SBATCH --mem-per-cpu=2500M
#SBATCH -J "qe_os_meric"
#SBATCH -A p_readex
##SBATCH --reservation=READEX
#SBATCH --output=qe_ori_sacct_meric.out
#SBATCH --error=qe_ori_sacct_meric.out

#####
# set to number of times to repeat experiments
REPEAT_COUNT=25
#####

#LOC=$(pwd)
#cd ..
LOG_FILE="sacct_meric_plain.out"
module purge
source ./readex_env/set_env_plain.source
##source $LOC/set_env_bem4i.source
cd input_data_small
i=1
rm -rf PLAIN_meric_v9_*
while [ $i -le $REPEAT_COUNT ]; do
  mkdir PLAIN_meric_v9_$i
  export MEASURE_RAPL_TARGET="PLAIN_meric_v9_$i"
   srun -N 1 -n 24 measure-rapl ${PWD}/../qe-6.1-scorep.noInst/PW/src/pw.x -i pw.in
  i=$(echo "$i + 1" | bc)
done
cd ..
module purge
source ./readex_env/set_env_meric.source
#source $LOC/set_env_bem4i.source
cd input_data_small
# SET MERIC OUTPUT
#export MERIC_COUNTERS=perfevent
export MERIC_MODE=3
export MERIC_DEBUG=0
export MERIC_CONTINUAL=1
export MERIC_AGGREGATE=0
export MERIC_DETAILED=0
export MERIC_NUM_THREADS=0
export MERIC_FREQUENCY=0
export MERIC_UNCORE_FREQUENCY=0
export MERIC_REGION_OPTIONS=./Energy_v3_meric.opts


i=1
rm -rf TUNED_meric_v9_*
while [ $i -le $REPEAT_COUNT ]; do
  mkdir TUNED_meric_v9_$i
  export MEASURE_RAPL_TARGET="TUNED_meric_v9_$i"
  srun -N 1 -n 24 measure-rapl ${PWD}/../qe-6.1-scorep.noInst/PW/src/pw.x -i pw.in
  i=$(echo "$i + 1" | bc)
done
 


div ()  # Arguments: dividend and divisor
{
        if [ $2 -eq 0 ]; then echo division by 0; exit; fi
        local p=12                            # precision
        local c=${c:-0}                       # precision counter
        local d=.                             # decimal separator
        local r=$(($1/$2)); echo -n $r        # result of division
        local m=$(($r*$2))
        [ $c -eq 0 ] && [ $m -ne $1 ] && echo -n $d
        [ $1 -eq $m ] || [ $c -eq $p ] && return
        local e=$(($1-$m))
        let c=c+1
        div $(($e*10)) $2
}

REPEAT_COUNT=25
#####
i=1
LC_NUMERIC="de_CH.UTF-8" 
total_energy_plain=0
total_time_plain=0
while [ $i -lt $((REPEAT_COUNT + 1)) ]; do
	
	for file in PLAIN_meric_v9_$i/*
  	do
	#echo $file
	#echo "i: " $i
    #values=$( tail -1 $file | awk -F'[ ,]' '{energy[$i]=$1+$2+$3+$4;time[$i]=$5;print "Energy:"energy[$i]; print "time:"time[$i] }' )
    energy_=$( tail -1 $file | awk -F'[ ,]' '{energy[$i]=$1+$2+$3+$4; print energy[$i] }' )
    time_=$( tail -1 $file | awk -F'[ ,]' '{time[$i]=$5; print time[$i] }' )
	energy[$i]=$energy_
	time[$i]=$time_	
	#echo $values
	#echo $energy_
	#echo $time_
	total_energy_plain=`echo ${energy[$i]} + $total_energy_plain | bc`
	total_time_plain=`echo ${time[$i]} + $total_time_plain | bc` 
	#echo "tot plain" $total_energy_plain
	
  done
	i=$((i+1))
done


avg_time_plain=$(echo "$total_time_plain / $((REPEAT_COUNT)) " | bc)
avg_energy_plain=$(echo "$total_energy_plain / $((REPEAT_COUNT))" | bc)

#Sigma Energy computation PLAIN
exp_e_suma_plain=0
for  j in `seq 1 $((REPEAT_COUNT))`; do
	#echo "j :"$j
	suma_e_plain=`echo ${energy[$j]} - $avg_energy_plain | bc` 
	exp_e_plain=$(echo "${suma_e_plain}^2" | bc)     
	exp_e_suma_plain=`echo $exp_e_suma_plain + $exp_e_plain | bc` 
	#echo "suma plain: "$suma_e_plain" "$exp_e_plain" "$exp_e_suma_plain
done

sigma_e_exp=$(echo "$total_energy_plain / $((REPEAT_COUNT))" | bc)
sigma_e_plain=$(echo "sqrt($sigma_e_exp)" | bc)
three=3
sigma3_e_plain=$(( $sigma_e_plain * $three))



#Sigma Time computation PLAIN
exp_t_suma_plain=0
for  j in `seq 1 $((REPEAT_COUNT))`; do
	#echo "j :"$j
	suma_t_plain=`echo ${time[$j]} - $avg_time_plain | bc` 
	exp_t_plain=$(echo "${suma_t_plain}^2" | bc)     
	exp_t_suma_plain=`echo $exp_t_suma_plain + $exp_t_plain | bc` 
	#echo "suma plain: "$suma_t_plain" "$exp_t_plain" "$exp_t_suma_plain
done

sigma_t_exp=$(echo "$total_time_plain / $((REPEAT_COUNT))" | bc)
sigma_t_plain=$(echo "sqrt($sigma_t_exp)" | bc)
three=3
sigma3_t_plain=$(( $sigma_t_plain * $three)) 
#echo " energy 3sigma"$sigma3_e_plain
#echo " ime 3sigma"$sigma3_t_plain

#REPEAT_COUNT=25
#####
i=1
total_energy_meric=0
total_time_meric=0
while [ $i -lt $((REPEAT_COUNT + 1)) ]; do
	
	for file in TUNED_meric_v9_$i/*
  	do
	#echo $file
	#echo $i
   # values=$( tail -1 $file | awk -F'[ ,]' '{energy[$i]=$1+$2+$3+$4;time[$i]=$5;print "Energy:"energy[$i]; print "time:"time[$i] }' )
    energy_=$( tail -1 $file | awk -F'[ ,]' '{energy[$i]=$1+$2+$3+$4; print energy[$i] }' )
    time_=$( tail -1 $file | awk -F'[ ,]' '{time[$i]=$5; print time[$i] }' )
	#echo "vslurd " $values
	energy[$i]=$energy_
	time[$i]=$time_
	#echo "Energy: "${energy[$i]}
	#echo "Time: "${time[$i]}
	total_energy_meric=`echo ${energy[$i]} + $total_energy_meric | bc` 
	total_time_meric=`echo ${time[$i]} + $total_time_meric | bc` 
	#echo "tot" $total_energy_meric
	
  done
	i=$((i+1))
done

avg_time_meric=$(echo "$total_time_meric / $((REPEAT_COUNT))" | bc)
avg_energy_meric=$(echo "$total_energy_meric / $((REPEAT_COUNT))" | bc)

#Sigma Energy computation meric
exp_e_suma_meric=0
for  j in `seq 1 $((REPEAT_COUNT))`; do
	#echo "j :"$j
	suma_e_meric=`echo ${energy[$j]} - $avg_energy_meric | bc` 
	exp_e_meric=$(echo "${suma_e_meric}^2" | bc)     
	exp_e_suma_meric=`echo $exp_e_suma_meric + $exp_e_meric | bc` 
	#echo "suma meric: "$suma_e_meric" "$exp_e_meric" "$exp_e_suma_meric
done

sigma_e_exp=$(echo "$total_energy_meric / $((REPEAT_COUNT))" | bc)
sigma_e_meric=$(echo "sqrt($sigma_e_exp)" | bc)
three=3
sigma3_e_meric=$(( $sigma_e_meric * $three)) 

#Sigma Time computation meric
exp_t_suma_meric=0
for  j in `seq 1 $((REPEAT_COUNT))`; do
	#echo "j :"$j
	suma_t_meric=`echo ${time[$j]} - $avg_time_meric | bc` 
	exp_t_meric=$(echo "${suma_t_meric}^2" | bc)     
	exp_t_suma_meric=`echo $exp_t_suma_meric + $exp_t_meric | bc` 
	#echo "suma meric: "$suma_t_meric" "$exp_t_meric" "$exp_t_suma_meric
done

sigma_t_exp=$(echo "$total_time_meric / $((REPEAT_COUNT))" | bc)
sigma_t_meric=$(echo "sqrt($sigma_t_exp)" | bc)
three=3
sigma3_t_meric=$(( $sigma_t_meric * $three)) 


#echo "sigma meric: "$sigma_t_meric
#echo "Three sigma: "$sigma3_t_meric
 



avg_time_meric=$(echo "$total_time_meric / $((REPEAT_COUNT))" | bc)
avg_energy_meric=$(echo "$total_energy_meric / $((REPEAT_COUNT))" | bc)
#avg_cpu_energy_plain=$(echo "$total_cpu_energy_plain " | bc)
#avg_cpu_energy_meric=$(echo "$total_cpu_energy_meric / $((REPEAT_COUNT))" | bc)

#Energy savings
#echo "Ev:"$avg_energy_plain
Esavings_meric=$(echo "$avg_energy_plain - $avg_energy_meric " | bc)
Esavings_meric=$(echo "$(div $Esavings_meric  $avg_energy_plain ) * 100" | bc)
Esavings_3_sigma_meric=$(echo "$(div $sigma3_e_meric $avg_energy_plain ) * 100" | bc)
#echo "EVs"$Esavings_meric, "sigma "$Esavings_3_sigma_meric
echo "" 2>&1 | tee ${LOG_FILE}
echo "Total Plain Time = $total_time_plain, Total Plain Energy = $total_energy_plain" 2>&1 | tee ${LOG_FILE}
echo "Total meric Time = $total_time_meric, Total meric Energy  = $total_energy_meric" 2>&1 | tee ${LOG_FILE}

echo "" 2>&1 | tee ${LOG_FILE}
echo "Average Plain Time = $avg_time_plain ± $sigma3_t_plain 3σ, Average meric Time = $avg_time_meric ± $sigma3_t_meric 3σ (3sigma)" 2>&1 | tee ${LOG_FILE}
echo "Average Plain Energy = $avg_energy_plain ± $sigma3_e_plain 3σ, Average meric Energy = $avg_energy_meric ± $sigma3_e_meric 3σ (3sigma) " 2>&1 | tee ${LOG_FILE}
echo "Energy savings QE meric: $Esavings_meric ± $Esavings_3_sigma_meric (3σ)" 2>&1 | tee ${LOG_FILE}
#echo "Average Plain CPU Energy = $avg_cpu_energy_plain, Average meric CPU Energy = $avg_cpu_energy_meric"



cp -r PLAIN_meric_v9_* ../.
cp -r TUNED_meric_v9_* ../.
cp SACCT_meric_results.log ../.
#rm -rf PLAIN_meric_v9_*
#rm -rf TUNED_meric_v9_*

