#!/bin/bash

#SBATCH --time=1:00:00   # walltime
#SBATCH --nodes=1  # number of processor cores (i.e. tasks)
##SBATCH --ntasks=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=24
#SBATCH --exclusive
#SBATCH --partition=haswell
#SBATCH --comment="no_monitoring"
#SBATCH --mem-per-cpu=2500M   # memory per CPU core
#SBATCH -A p_readex
#SBATCH --reservation=READEX
#SBATCH -J "qe_rdd"   # job name
#SBATCH --output=qe_rdd_manual.out
#SBATCH --error=qe_rdd_manual.out

###############################################################################

echo "run RDD manual begin."

module purge
source ./readex_env/set_env_rdd.source

ROOT_DIR=${PWD}

rm -rf scorep-*
rm -f readex_config_manual.xml

cd input_data_small

RDD_t=0.1 #100ms
RDD_p=mainpwscf
RDD_c=10
RDD_v=10
RDD_w=10
RDD_r=readex_config_ptf_manual

export SCOREP_PROFILING_FORMAT=cube_tuple
#export SCOREP_METRIC_PAPI=PAPI_TOT_INS,PAPI_L3_TCM

#srun -N 1 -n 24 ${ROOT_DIR}/qe-6.1-scorep.noInst/PW/src/pw.x -i ZnO.in
mpirun -n 24 ${ROOT_DIR}/qe-6.1-scorep.Inst/PW/src/pw.x -i pw.in
mv scorep-* ../.
cd ..

readex-dyn-detect -t $RDD_t -p $RDD_p -c $RDD_c -v $RDD_v -w $RDD_w -r $RDD_r scorep-*/profile.cubex


echo "RDD result = $?"

echo "run RDD done."

